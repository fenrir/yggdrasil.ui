package org.fenrir.yggdrasil.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.JOptionPane;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.WorkspaceDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.exception.PreferenceException;
import org.fenrir.yggdrasil.core.service.IWorkspaceAdministrationService;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
@SuppressWarnings("serial")
public class SwitchWorkspaceAction extends AbstractAction
{
    private Logger log = LoggerFactory.getLogger(SwitchWorkspaceAction.class);

    private String workspaceFolder;
    
    public SwitchWorkspaceAction()
    {
        super("Sel.leccionar...");
    }
    
    public SwitchWorkspaceAction(String title, String workspaceFolder)
    {
        super(title);
        this.workspaceFolder = workspaceFolder;
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {      
        IWorkspaceAdministrationService workspaceService = (IWorkspaceAdministrationService)ApplicationContext.getInstance().getRegisteredComponent(IWorkspaceAdministrationService.class);
        
        // No s'ha indicat cap directori. S'obre la finestra de sel.lecció
        if(StringUtils.isBlank(workspaceFolder)){
            WorkspaceDialog dialog = new WorkspaceDialog();
            int returnCode = dialog.open();
            
            if(returnCode==WorkspaceDialog.DIALOG_OK){
                workspaceFolder = dialog.getWorkspacePath();
                // S'afegeix al registre el nou workspace
                try{
                    workspaceService.addWorkspace(workspaceFolder, dialog.isDefaultWorkspace());
                }
                catch(PreferenceException e){
                    log.error("Error al carregar workspace: {}", e.getMessage(), e);
                    JOptionPane.showMessageDialog(null, "Error al carregar workspace: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
            else{
                return;
            }
        }

        // Una vegada triat el workspace, s'obre la connexió a BDD.
        try{
            // Es guarden els valors a la configuració i es carrega el workspace. Això notifica l'event de càrrega de workspace
            boolean workspaceLoaded = workspaceService.isWorkspaceLoaded();                
            // Si no hi ha workspace carregat es segueix l'execució normal
            if(!workspaceLoaded){
                workspaceService.loadWorkspace(workspaceFolder);
            }
            // En el cas que hi hagi workspace carregat, es reinicia l'aplicació
            else{
                if(log.isDebugEnabled()){
                    log.debug("Especificat reinici en el workspace {}", workspaceFolder);
                }
                workspaceService.setRestartWorkspace(workspaceFolder);
                ApplicationWindowManager.getInstance().closeWindow();
            }
        }
        catch(PreferenceException e){
            log.error("Error al carregar workspace: {}", e.getMessage(), e);
            JOptionPane.showMessageDialog(null, "Error al carregar workspace: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        catch(ApplicationException e){
            log.error("Error al carregar workspace: {}", e.getMessage(), e);
            JOptionPane.showMessageDialog(null, "Error al carregar workspace: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }            
    }
}