package org.fenrir.yggdrasil.ui.action;

import java.awt.Dimension;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.TaskProgressDialog;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateActions;
import org.fenrir.yggdrasil.core.descriptor.ArtifactUpdateDescriptor;
import org.fenrir.yggdrasil.core.service.IApplicationUpdateService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
public class DownloadUpdatesAction extends Action
{    
    private final Logger log = LoggerFactory.getLogger(DownloadUpdatesAction.class);
    
    private TaskProgressDialog dialog;
    
    @Override
    public void actionPerformed() 
    {
        AbstractWorker<Boolean, String> worker = new AbstractWorker<Boolean, String>() 
        {
            @Override
            protected Boolean doInBackground() throws Exception 
            {
                IApplicationUpdateService updateService = (IApplicationUpdateService)ApplicationContext.getInstance().getRegisteredComponent(IApplicationUpdateService.class);
                ArtifactUpdateActions actions = updateService.readActionsFile();
                for(ArtifactUpdateDescriptor descriptor:actions.getActions()){
                    if(ArtifactUpdateDescriptor.TYPE_CREATE_ARTIFACT.equals(descriptor.getType())
                            || ArtifactUpdateDescriptor.TYPE_UPDATE_ARTIFACT.equals(descriptor.getType())){
                        String url = descriptor.getUrl();
                        String filename = descriptor.getDestinationPath();
                        publish("Descarregant " + url);
                        updateService.downloadFile(url, filename);                         
                    }
                }
                // S'actualitza el fitxer descriptor per indicar que s'ha d'actualitzar a la pròxima arrancada de l'aplicació
                actions.setUpdatable(true);
                updateService.saveActionsFile(actions);
                
                return Boolean.TRUE;
            }
            
            /**
             * Mètode que s'executarà al finalitzar la tasca en el Thread d'event de la UI. Serà l'encarregat
             * d'actualitzar la UI de la finestra de diàleg amb el resultat final.
             */
            @Override
            protected void done() 
            {
                super.done();
                try{
                    // En aquest cas el mètode get no bloqueja perquè el procés ja ha finalitzat
                    if(get()){
                        dialog.setMessage("Actualitzacions descarregades. Reiniciar l'aplicació perquè tinguin efecte", 
                                TaskProgressDialog.MSG_TYPE_INFO);                        
                    }
                } 
                catch(Exception e){
                    log.error("Error descarregant actualitzacions: {}", e.getMessage(), e);
                    // Es tanca la finestra de progrés i es mostra l'error
                    dialog.setVisible(false);
                    dialog.dispose();
                    ApplicationWindowManager.getInstance().displayErrorMessage("Error descarregant les actualitzacions", e);
                }
            }
        };        
        
        try{
            dialog = new TaskProgressDialog("Descarregant actualitzacions", worker, new Dimension(500, 180));
            dialog.open();        
        }
        catch(Exception e){
            log.error("Error descarregant actualitzacions: {}", e.getMessage(), e);
            // Es tanca la finestra de progrés i es mostra l'error
            dialog.setVisible(false);
            dialog.dispose();
            ApplicationWindowManager.getInstance().displayErrorMessage("Error descarregant les actualitzacions", e);            
        }
    }   
}
