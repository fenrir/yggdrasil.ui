package org.fenrir.yggdrasil.ui.action;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140411
 */
public abstract class Action extends AbstractAction 
{
	private static final long serialVersionUID = -8886245644305974231L;

	protected ActionEvent event;
	
	public Action()
    {
        
    }
    
    public Action(String title)
    {
        super(title);
    }
    
    @Override
    public void actionPerformed(ActionEvent event) 
    {
    	this.event = event;
        actionPerformed();
    }
    
    public abstract void actionPerformed();
}
