package org.fenrir.yggdrasil.ui.module;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.TypeLiteral;
import com.google.inject.matcher.AbstractMatcher;
import com.google.inject.name.Names;
import com.google.inject.spi.InjectionListener;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationTray;
import org.fenrir.yggdrasil.ui.event.ApplicationErrorNotification;
import org.fenrir.yggdrasil.ui.event.ApplicationUpdatedNotification;
import org.fenrir.yggdrasil.ui.mvc.AbstractPerspective;
import org.fenrir.yggdrasil.ui.mvc.AbstractView;
import org.fenrir.yggdrasil.ui.mvc.EmptyPerspective;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140724
 */
public class UIStartupModule extends AbstractModule
{
	private final Logger log = LoggerFactory.getLogger(UIStartupModule.class);
    
    @Override
    protected void configure()
    {
    	/* Notifications */
    	bind(IApplicationNotification.class)
		        .annotatedWith(Names.named(NotificationEventConstants.NOTIFICATION_ERROR))
		        .to(ApplicationErrorNotification.class);
		bind(IApplicationNotification.class)
		        .annotatedWith(Names.named(NotificationEventConstants.NOTIFICATION_APPLICATION_UPDATED))
		        .to(ApplicationUpdatedNotification.class);

    	bind(ApplicationTray.class).in(Singleton.class);
    	
    	// Perspectiva buida per defecte fins que arranca l'aplicació
        bind(AbstractPerspective.class)
			.annotatedWith(Names.named(EmptyPerspective.ID))
			.to(EmptyPerspective.class)
			.in(Singleton.class);
		
		/* Listeners injecció */
    	// Listener per inicialitzar les vistes després de la seva injecció
        bindListener(new AbstractMatcher<TypeLiteral<?>>() 
        {
            @Override
            public boolean matches(TypeLiteral<?> typeLiteral) 
            {
                return AbstractView.class.isAssignableFrom(typeLiteral.getRawType());
            }
        }, new ViewTypeListener());
        // Listener per inicialitzar les perspectives després de la seva injecció
        bindListener(new AbstractMatcher<TypeLiteral<?>>() 
        {
            @Override
            public boolean matches(TypeLiteral<?> typeLiteral) 
            {
                return AbstractPerspective.class.isAssignableFrom(typeLiteral.getRawType());
            }
        }, new PerspectiveTypeListener());
    }
    
    class ViewTypeListener implements TypeListener
    {
        @Override
        public <I> void hear(final TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) 
        {
            typeEncounter.register(new InjectionListener<I>() 
            {
                /**
                 * Mètode executat una vegada Guice ha injectat tots els membres a l'objecte
                 */
                @Override
                public void afterInjection(I i) 
                {
                    if(log.isDebugEnabled()){
                        log.debug("Executant post inicialització de la vista {}", i.getClass().getName());
                    }
                    
                    AbstractView<?> view = (AbstractView<?>)i;
                    view.createView();
                }
            });
        }
    }
    
    class PerspectiveTypeListener implements TypeListener
    {
        @Override
        public <I> void hear(final TypeLiteral<I> typeLiteral, TypeEncounter<I> typeEncounter) 
        {
            typeEncounter.register(new InjectionListener<I>() 
            {
                /**
                 * Mètode executat una vegada Guice ha injectat tots els membres a l'objecte
                 */
                @Override
                public void afterInjection(I i) 
                {
                    if(log.isDebugEnabled()){
                        log.debug("Executant post inicialització de la perspectiva {}", i.getClass().getName());
                    }
                    
                    AbstractPerspective perspective = (AbstractPerspective)i;
                    perspective.createPerspective();
                }
            });
        }
    }
}
