package org.fenrir.yggdrasil.ui.wizard;

import java.util.Collection;
import java.util.LinkedHashMap;
import org.fenrir.yggdrasil.ui.dialog.WizardDialog;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
public abstract class AbstractWizard 
{	
    protected String title;
    protected LinkedHashMap<String, AbstractWizardPage<? extends AbstractWizard>> vPages = new LinkedHashMap<String, AbstractWizardPage<? extends AbstractWizard>>();
    protected int currentPage = 0;

    protected WizardDialog<? extends AbstractWizard> dialog;

    public String getTitle()
    {
        return title;
    }
	
    public void setTitle(String title)
    {
        this.title = title;
    }
	
    public AbstractWizardPage<? extends AbstractWizard> getPage(String id)
    {
        return vPages.get(id);
    }
	
    public AbstractWizardPage<? extends AbstractWizard> getCurrentPage()
    {
        if(vPages.size()>currentPage){
            return (AbstractWizardPage)vPages.values().toArray()[currentPage];
        }
        else{
            return null;
        }
    }
    
    public String getCurrentPageId()
    {
        AbstractWizardPage<? extends AbstractWizard> page = getCurrentPage();
        return page!=null ? page.getId() : null;
    }
    
    public AbstractWizardPage<? extends AbstractWizard> getNextPage()
    {
        if(vPages.size()>currentPage+1){
            return (AbstractWizardPage)vPages.values().toArray()[currentPage+1];
        }
        else{
            return null;
        }
    }
    
    public String getNextPageId()
    {
        AbstractWizardPage<? extends AbstractWizard> page = getNextPage();
        return page!=null ? page.getId() : null;
    }
		
    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected void addPage(AbstractWizardPage page)
    {		
        vPages.put(page.getId(), page);				
    }		

    public AbstractWizardPage<? extends AbstractWizard> nextPage()
    {
        currentPage++;
        return getCurrentPage();		
    }
	
    public AbstractWizardPage<? extends AbstractWizard> previousPage()
    {
        currentPage--;
        return getCurrentPage();	
    }		
	
    public Collection<AbstractWizardPage<? extends AbstractWizard>> getAllPages()
    {
        return vPages.values();
    }
	
    protected abstract boolean canPerformPrevious();	

    protected abstract boolean canPerformNext();	

    protected abstract boolean canPerformFinalize();	

    public abstract boolean performPrevious();	

    public abstract boolean performNext();	

    public abstract boolean performFinalize();	

    public abstract boolean performCancel();
	
    public boolean validateCurrentPage()
    {
        boolean isValid = getCurrentPage().validatePage();		
        dialog.setBackwardButtonEnabled(canPerformPrevious());
        if(isValid){
            dialog.setForwardButtonEnabled(canPerformNext());
            dialog.setFinalizeButtonEnabled(canPerformFinalize());

            // Missatge per defecte en el cas que no hi hagi error
        showInfoMessage(getCurrentPage().getDescription());
        }
        else{
            dialog.setForwardButtonEnabled(false);
            dialog.setFinalizeButtonEnabled(false);
        }

        return isValid;
    }
	
    public void setWizardDialog(WizardDialog<? extends AbstractWizard> dialog)
    {
        this.dialog = dialog;
    }

    public void showErrorMessage(String message)
    {
        dialog.setMessage(message, WizardDialog.MSG_TYPE_ERROR);
    }

    public void showWarnMessage(String message)
    {
        dialog.setMessage(message, WizardDialog.MSG_TYPE_WARN);
    }

    public void showInfoMessage(String message)
    {
        dialog.setMessage(message, WizardDialog.MSG_TYPE_INFO);
    }

    public void clearMessage()
    {
        dialog.clearMessage();
    }

    public String getIcon()
    {
        return null;
    }
}