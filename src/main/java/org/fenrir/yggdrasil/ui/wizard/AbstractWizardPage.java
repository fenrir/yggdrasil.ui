package org.fenrir.yggdrasil.ui.wizard;

import java.awt.GridLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
@SuppressWarnings("serial")
public abstract class AbstractWizardPage<T extends AbstractWizard> extends JPanel 
{
    protected String title;
    protected String description;

    protected T wizard;
	
    public AbstractWizardPage(T wizard)
    {
        this.wizard = wizard;

        GridLayout layout = new GridLayout(1, 1);
        setLayout(layout);		
        add(createContents());
    }
	
    public AbstractWizardPage(String title, String description, T wizard)
    {
        this(wizard);

        this.title = title;
        this.description = description;
    }

    public abstract String getId();	

    protected abstract JComponent createContents();

    public String getTitle() 
    {
        return title;
    }

    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getDescription() 
    {
        return description;
    }

    public void setDescription(String description) 
    {
        this.description = description;
    }

    public void setWizard(T wizard)
    {
        this.wizard = wizard;
    }

    public abstract boolean validatePage();
}