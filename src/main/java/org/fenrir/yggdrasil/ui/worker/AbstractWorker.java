package org.fenrir.yggdrasil.ui.worker;

import java.util.ArrayList;
import java.util.List;
import javax.swing.SwingWorker;
import org.fenrir.yggdrasil.ui.event.IWorkerUpdateListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131205
 */
public abstract class AbstractWorker<T, U> extends SwingWorker<T, U> 
{
    protected List<IWorkerUpdateListener<U>> listeners = new ArrayList<IWorkerUpdateListener<U>>();
	
    public void addProcessListener(IWorkerUpdateListener<U> listener)
    {
        listeners.add(listener);
    }
	
    public void removeProcessListener(IWorkerUpdateListener<U> listener)
    {
        listeners.remove(listener);
    }
	
    public void removeAllProcessListeners()
    {
        listeners.clear();
    }
	
    /**
     * Mètode que s'executarà al finalitzar la tasca en el Thread d'event de la UI. 
     * La implementació base d'aquesta clase s'encarrega d'avisar als listeners registrats que el procés ha acabat
     */
    @Override
    protected void done() 
    {		
        for(IWorkerUpdateListener<U> listener:listeners){
            listener.done();
        }
    }        
}
