package org.fenrir.yggdrasil.ui.widget;

import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.DocumentListener;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20121215
 */
public class AdvancedTextField extends JPanel
{
	private static final long serialVersionUID = -8642962916123247748L;
	
	protected JTextField inputText;
    protected JButton bOption;

    public AdvancedTextField(String iconPath)
    {
        createContents(iconPath);
    }

    private void createContents(String iconPath)
    {
        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
        inputText = new JTextField();
        add(inputText);
        bOption = new JButton();
        bOption.setIcon(new ImageIcon(getClass().getResource(iconPath)));
        bOption.setSize(16, 16);
        add(bOption);

        setBackground(inputText.getBackground());
        setBorder(inputText.getBorder());
        inputText.setBorder(null);
    }

    public String getText()
    {
        return inputText.getText();
    }

    public void setText(String text)
    {
        inputText.setText(text);
        inputText.setToolTipText(text);
    }

    @Override
    public void setEnabled(boolean enabled)
    {
        inputText.setEnabled(enabled);
        bOption.setEnabled(enabled);
    }

    public void setEditable(boolean editable)
    {
        inputText.setEditable(editable);
        inputText.setOpaque(editable);
        bOption.setEnabled(editable);
    }

    @Override
    public void addKeyListener(KeyListener listener)
    {
        inputText.addKeyListener(listener);
    }

    @Override
    public void removeKeyListener(KeyListener listener)
    {
        inputText.removeKeyListener(listener);
    }

    public void addActionListener(ActionListener listener)
    {
        bOption.addActionListener(listener);
    }

    public void removeActionListener(ActionListener listener)
    {
        bOption.removeActionListener(listener);
    }

    public void addDocumentListener(DocumentListener listener)
    {
        inputText.getDocument().addDocumentListener(listener);
    }

    public void removeDocumentListener(DocumentListener listener)
    {
        inputText.getDocument().removeDocumentListener(listener);
    }
}