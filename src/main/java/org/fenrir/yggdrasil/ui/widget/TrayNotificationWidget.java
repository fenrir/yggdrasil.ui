package org.fenrir.yggdrasil.ui.widget;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseListener;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import org.jdesktop.jxlayer.JXLayer;
import org.jdesktop.jxlayer.plaf.AbstractLayerUI;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class TrayNotificationWidget
{
    private Icon icon;
    private int notificationCount;
    private JLabel label;
    private JXLayer layer;
    
    public TrayNotificationWidget(Icon icon)
    {
        this.icon = icon;
        createContents();
    }
    
    private void createContents()
    {
        label = new JLabel(icon);
        label.setPreferredSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        label.setMaximumSize(new Dimension(icon.getIconWidth(), icon.getIconHeight()));
        layer = new JXLayer(label);
        layer.setPreferredSize(new Dimension(icon.getIconWidth() + 10, icon.getIconHeight()));
        layer.setMaximumSize(new Dimension(icon.getIconWidth() + 10, icon.getIconHeight()));
        layer.setUI(new AbstractLayerUI() 
        {
            @Override  
            protected void paintLayer(Graphics2D g2, JXLayer layer) 
            {
                // Es pinta la capa tal qual és
                super.paintLayer(g2, layer);             
                if(notificationCount>1){
                    // Color del comptador de notifications
                    g2.setColor(Color.RED);
                    g2.fillRoundRect(icon.getIconWidth()-5, 0, 13, 13, 4, 4);
                    g2.setFont(new Font(Font.DIALOG, Font.PLAIN, 10));
                    g2.setColor(Color.WHITE);
                    g2.drawRoundRect(icon.getIconWidth()-5, 0, 13, 13, 4, 4);
                    int coordX = icon.getIconWidth();
                    String strCount = new Integer(notificationCount).toString();
                    if(strCount.length()>1){
                        coordX -= 3;
                    }
                    g2.drawString(strCount, coordX, 10);
                }
            }
        });
    }
    
    public JComponent getWidget()
    {
        return layer;
    }

    public void addMouseListener(MouseListener listener) 
    {
        label.addMouseListener(listener);
    }     

    public synchronized void removeMouseListener(MouseListener listener)
    {
        label.removeMouseListener(listener);
    }
    
    public int getNotificationCount()
    {
        return notificationCount;
    }
    
    public void setNotificationCount(int notificationCount)
    {
        this.notificationCount = notificationCount;
    }
    
    public void incrementNotificationCount()
    {
        notificationCount++;
    }
    
    public void decrementNotificationCount()
    {
        notificationCount--;
    }
}
