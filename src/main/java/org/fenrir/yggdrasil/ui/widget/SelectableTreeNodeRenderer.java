package org.fenrir.yggdrasil.ui.widget;

import java.awt.Component;
import java.awt.Dimension;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.tree.TreeCellRenderer;
import org.pushingpixels.substance.api.SubstanceLookAndFeel;
import org.pushingpixels.substance.internal.utils.SubstanceStripingUtils;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class SelectableTreeNodeRenderer extends JPanel implements TreeCellRenderer
{
	private static final long serialVersionUID = 6542474546067240596L;
	
	private TristateCheckBox check;
    private JLabel label;

    public SelectableTreeNodeRenderer() 
    {
        setLayout(null);
        add(check = new TristateCheckBox());
        add(label = new JLabel());
        check.setBackground(UIManager.getColor("Tree.textBackground"));
        label.setForeground(UIManager.getColor("Tree.textForeground"));
    }
	
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean isSelected, boolean expanded, boolean leaf, int row, boolean hasFocus) 
    {
        String stringValue = tree.convertValueToText(value, isSelected, expanded, leaf, row, hasFocus);
        setEnabled(tree.isEnabled());
        
        if(value instanceof SelectableTreeNode){
            check.setState(((SelectableTreeNode)value).getState());
        }

        label.setFont(tree.getFont());
        label.setText(stringValue);      
        
        if(SubstanceLookAndFeel.isCurrentLookAndFeel()){
            SubstanceStripingUtils.applyStripedBackground(tree, row, this);
        }            
        if(check.isSelected()){
            setForeground(UIManager.getColor("Tree.selectionForeground"));
            setBackground(UIManager.getColor("Tree.selectionBackground"));
        }
        
        return this;
    }

    @Override
    public Dimension getPreferredSize() 
    {
        Dimension d_check = check.getPreferredSize();
        Dimension d_label = label.getPreferredSize();
        
        return new Dimension(d_check.width + d_label.width + 2, (d_check.height < d_label.height ? d_label.height : d_check.height));
    }

    @Override
    public void doLayout() 
    {
        Dimension checkDim = check.getPreferredSize();
        Dimension labelDim = label.getPreferredSize();
        int checkY = 0;
        int labelY = 0;
        if(checkDim.height<labelDim.height){
            checkY = (labelDim.height - checkDim.height) / 2;
        } 
        else{
            labelY = (checkDim.height - labelDim.height) / 2;
        }

        check.setBounds(0, checkY, checkDim.width, checkDim.height);
        label.setBounds(checkDim.width + 2, labelY, labelDim.width, labelDim.height);
    }
}
