package org.fenrir.yggdrasil.ui.widget;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.LayoutStyle;
import javax.swing.event.MouseInputAdapter;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.fenrir.yggdrasil.ui.ApplicationTray;

/**
 * TODO v1.0 TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class TrayPopupEntryWidget extends JPanel
{
	private static final long serialVersionUID = -858246842349087240L;
	
	private ApplicationTray tray;
    private TrayPopupEntry entry;
    
    private JLabel lIcon;
    private JLabel lMessage;
    private JLabel lTimestamp;
//  private JProgressBar progressBar;
    
    public TrayPopupEntryWidget(ApplicationTray tray, TrayPopupEntry entry)
    {
        this.tray = tray;
        this.entry = entry;
        createContents();
    }
    
    private void createContents()
    {        
        String htmlMessage = "<html>" + entry.getMessage().replaceAll("\n", "<br/>") + "</html>";
        lMessage = new JLabel(htmlMessage);
        if(entry.getAction()!=null){
            lMessage.setToolTipText("Doble click per veure");
            lMessage.addMouseListener(new MouseInputAdapter() 
            {
                @Override
                public void mouseClicked(MouseEvent event) 
                {
                    if(event.getClickCount()>1){
                        entry.getAction().actionPerformed();
                    }
                }                
            });
        }
        
        JSeparator separator = new JSeparator();
        // TODO Crear panell progressBar / Timestamp
//      progressBar = new JProgressBar();
        String strTimestamp = new SimpleDateFormat("HH:mm:ss").format(new Date());
        lTimestamp = new JLabel(strTimestamp);

        lIcon = new JLabel(entry.getIcon());
        lIcon.setMaximumSize(new Dimension(25, 25));
        lIcon.setMinimumSize(new Dimension(25, 25));
        lIcon.setPreferredSize(new Dimension(25, 25));

        JButton bDiscard = new JButton(new ImageIcon(getClass().getResource("/org/fenrir/yggdrasil/ui/icons/remove_22.png")));
        bDiscard.setMaximumSize(new Dimension(25, 25));
        bDiscard.setMinimumSize(new Dimension(25, 25));
        bDiscard.setPreferredSize(new Dimension(25, 25));
        bDiscard.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent event) 
            {                      
                tray.discardNotification(entry);
            }
        });

        GroupLayout layout = new GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addComponent(separator, GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lIcon, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    // TODO Aquí anirà el panell amb la progressBar / timestamp. Per ara només es mostrarà el timestamp
                    .addComponent(lTimestamp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lMessage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bDiscard, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lIcon, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bDiscard, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lMessage, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        // TODO Aquí anirà el panell amb la progressBar / timestamp. Per ara només es mostrarà el timestamp
                        .addComponent(lTimestamp, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    )
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(separator, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            )                
        );
    }    
}
