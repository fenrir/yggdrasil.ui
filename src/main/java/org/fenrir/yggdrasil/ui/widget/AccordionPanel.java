package org.fenrir.yggdrasil.ui.widget;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * Component que proporciona una funcionalitat semblant a un JTabbedPane però que enlloc
 * de mantenir tabs utilitza un sistema de barres per controlar el component visible en cada moment 
 * @author Antonio Archilla Nava
 * @version v0.1.20131215 
 */
@SuppressWarnings("serial")
public class AccordionPanel extends JPanel implements ActionListener 
{
    /*----------*
     * ATRIBUTS *
     *----------*/
    /* Panell superior: Conté els botons mostrats a la part superior del panell */
    private JPanel topPanel = new JPanel(new GridLayout(1, 1));

    /* Panell inferior: Conté els botons mostrats a la part inferior del panell */
    private JPanel bottomPanel = new JPanel(new GridLayout(1, 1));

    /* Referències a les barres del panell preservant l'ordre d'inserció */
    private Map<String, BarInfo> bars = new LinkedHashMap<String, BarInfo>();

    /* Index del component actualment visible */
    private int visibleBar = 0;

    /* Referència al component actualment visible */
    private JComponent visibleComponent = null;

    /*--------------*
     * CONSTRUCTORS *
     *--------------*/
    /**
     * Creates a new JOutlookBar; after which you should make repeated calls to
     * addBar() for each bar
     */
    public AccordionPanel() 
    {
        this.setLayout(new BorderLayout());
        this.add(topPanel, BorderLayout.NORTH);
        this.add(bottomPanel, BorderLayout.SOUTH);
    }

    /*---------*
     * MÈTODES *
     *---------*/
    /**
     * Afegeix un nou component al panell a continuació de l'últim inserit amb el nom especificat
     * @param name String - Nom que es mostrará al panell per identificar el component
     * @param component JComponent - Component a afegir al panell
     */
    public void addBar(String name, JComponent component) 
    {
        BarInfo barInfo = new BarInfo(name, component);
        addBar(barInfo);
    }

    /**
     * Afegeix un nou component al panell a continuació de l'últim inserit amb el nom i icona especificades
     * @param name String - Nom que es mostrará al panell per identificar el component
     * @param icon Icon - Icona que es mostrará juntamente amb el nom a la barra corresponent al component afegit
     * @param component JComponent - Component a afegir al panell
     */	
    public void addBar(String name, Icon icon, JComponent component) 
    {
        BarInfo barInfo = new BarInfo(name, icon, component);
        addBar(barInfo);
    }
	
    /**
     * Mètode genèric d'inserció d'un nou component a partir de les dades de la barra 
     * @param name String - Nom que es mostrará al panell per identificar el component
     * @param component JComponent - Component a afegir al panell
     */
    protected void addBar(BarInfo barInfo)
    {
        barInfo.getButton().addActionListener(this);
        this.bars.put(barInfo.getName(), barInfo);
        render();
    }

    /**
     * Elimina la barra especificada del panell
     * @param name String - Nom de la barra a eliminar
     */
    public void removeBar(String name) 
    {
        this.bars.remove(name);
        render();
    }

    /**
     * Retorna l'índex de la barra actualment visible
     * @return int - índex de la barra actualment visible
     */
    public int getVisibleBar() 
    {
        return this.visibleBar;
    }

    /**
     * Mètode que canvia la barra visible a partir de l'índex especificat 
     * @param visibleBar int - Índex del component que es vol fer visible (0 a size-1)
     */
    public void setVisibleBar(int visibleBar) 
    {
        if(visibleBar>0 && visibleBar<this.bars.size()-1){
            this.visibleBar = visibleBar;
            render();
        }
    }

    /**
     * Mètode de renderitzat. Reconstrueix el panell a partir dels components continguts i els panells
     * superior i inferior que contenen les barres de sel.lecció. En acabar, fa visible el component 
     * actualment sel.leccionat 
     */
    public void render() 
    {
        // Es calcula el nombre de barres totals a la part inferior i superior
        int totalBars = this.bars.size();
        int topBars = this.visibleBar + 1;
        int bottomBars = totalBars - topBars;

        Iterator<String> itr = this.bars.keySet().iterator();

        // Es pinten les barres superiors: S'eliminen tots els components, es reestableixen els GridLayouts
        // per contenir el nombre correcte de barres, s'afegeixen les noves i es revalida el layout
        this.topPanel.removeAll();
        GridLayout topLayout = (GridLayout)this.topPanel.getLayout();
        topLayout.setRows(topBars);
        BarInfo barInfo = null;
        for (int i=0; i<topBars; i++){
            String barName = (String) itr.next();
            barInfo = (BarInfo)this.bars.get(barName);
            this.topPanel.add(barInfo.getButton());
        }
        this.topPanel.validate();

        // Es pinta el component central: S'elimina el component actual, si es que n'hi ha, 
        // i es fa visible el nou component sel.leccionat
        if(this.visibleComponent!=null){
            this.remove(this.visibleComponent);
        }
        this.visibleComponent = barInfo.getComponent();
        this.add(visibleComponent, BorderLayout.CENTER);

        // Es pinten les barres inferiors: S'eliminen tots els components, es reestableixen els GridLayouts
        // per contenir el nombre correcte de barres, s'afegeixen les noves i es revalida el layout		
        this.bottomPanel.removeAll();
        GridLayout bottomLayout = (GridLayout)this.bottomPanel.getLayout();
        bottomLayout.setRows(bottomBars);
        for (int i=0; i<bottomBars; i++) {
            String barName = (String) itr.next();
            barInfo = (BarInfo) this.bars.get(barName);
            this.bottomPanel.add(barInfo.getButton());
        }
        this.bottomPanel.validate();

        // Es validen tots els components del panell causant un re-layout dels subcomponents
        validate();
    }

    /**
     * Mètode invocat en sel.leccionar una barra
     * @param event ActionEvent - Event de sel.lecció
     */
    public void actionPerformed(ActionEvent event) 
    {
        int currentBar = 0;
        for(String barName:bars.keySet()){
            BarInfo barInfo = (BarInfo) this.bars.get(barName);
            if(barInfo.getButton()==event.getSource()){
                // trobat el component a mostrar
                this.visibleBar = currentBar;
                render();
                return;
            }
            currentBar++;
        }		
    }

    /*
     * Classe interna que manté la informació individual de cada una de les barres 
     * Aquesta informació inclou el nom, el botó associat per realitzar la sel.lecció del component visible
     * i el component propiament dit
     */
    private class BarInfo 
    {
        /* Nom de la barra */		 
        private String name;

        /* Botó del sel.lecció del component visible */
        private JButton button;

        /* Component */
        private JComponent component;

        /**
         * Creadora d'una nova barra a partir del seu nom i el component que la implementa
         * @param name String - Nom donat a la barra
         * @param component JComponent - Component a mostrar si es sel.lecciona la barra
         */
        public BarInfo(String name, JComponent component) 
        {
            this.name = name;
            this.component = component;
            this.button = new JButton(name);
        }

        /**
         * Creadora d'una nova barra a partir del seu nom, de la icona associada 
         * i el component que la implementa
         * @param name String - Nom donat a la barra
         * @param icon Icon - Icona associada que acompanyarà al nom en el botó de sel.lecció
         * @param component JComponent - Component a mostrar si es sel.lecciona la barra
         */
        public BarInfo(String name, Icon icon, JComponent component) 
        {
            this.name = name;
            this.component = component;
            this.button = new JButton(name, icon);
        }

        /**
         * Getter del nom de la barra
         * @return String - El nom de la barra
         */
        public String getName() 
        {
            return this.name;
        }

        /**
         * Setter del nom de la barra
         * @param name String - El nou nom de la barra
         */
        public void setName(String name) 
        {
            this.name = name;
        }

        /**
         * Getter de l'objecte Button associat al component
         * @return JButton - Botó que realitza la sel.lecció del component associat a la barra
         */
        public JButton getButton() 
        {
            return this.button;
        }

        /**
         * Getter del component associat a la barra
         * @return JComponent - Component que es mostrarà al sel.lecciónar la barra
         */		
        public JComponent getComponent() 
        {
            return this.component;
        }
    }
}