package org.fenrir.yggdrasil.ui.widget;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.dialog.ElementSelectionDialog;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.provider.IElementSearchProvider;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131230
 */
public class ElementSearchTextField<T> extends AdvancedTextField 
{
	private static final long serialVersionUID = -1443167684314432321L;
	
	private Class<? extends IElementSearchProvider<?>> providerClass;
	private T selectedElement;
	
	public ElementSearchTextField(Class<? extends IElementSearchProvider<?>> providerClass)
	{
		super("/org/fenrir/yggdrasil/ui/icons/search_16.png");
		this.providerClass = providerClass;
		
		addActionListener(new ActionListener() 
        {
			@Override
			public void actionPerformed(ActionEvent event) 
			{
				openSelection(getText());
			}
		});
		
		// Inicialment el color del text indicarà que no s'ha sel.leccionat cap repositori
		inputText.setForeground(Color.RED);
		
		addKeyListener(new KeyAdapter()
    	{
			@Override
			public void keyPressed(KeyEvent event)
			{
				int keyPressed = event.getKeyCode();
        		if(keyPressed==KeyEvent.VK_ENTER){
        			openSelection(getText());
        		}
        		else if(keyPressed!=KeyEvent.VK_TAB){
					// En el moment que s'insereix text, l'element sel.lecciónat queda invalidat
					selectedElement = null;
					inputText.setForeground(Color.RED);
				}
			}
    	});
	}
	
	public boolean isFieldValid()
	{
		return selectedElement!=null;
	}
	
	public T getSelectedElement()
	{
		return selectedElement;
	}
	
	public void setSelectedElement(T selectedElement)
	{
		this.selectedElement = selectedElement;
		inputText.setForeground(Color.BLACK);
		if(selectedElement!=null){
			setText(selectedElement.toString());
		}
		else{
			setText(null);
		}
	}

	private void openSelection(String text)
	{
		ElementSelectionDialog<T> dialog;
		if(StringUtils.isNotBlank(text)){
			dialog = new ElementSelectionDialog<T>(ApplicationWindowManager.getInstance().getMainWindow(), 
					(IElementSearchProvider<T>)ApplicationContext.getInstance().getRegisteredComponent(providerClass),
					text);
		}
		else{
			dialog = new ElementSelectionDialog<T>(ApplicationWindowManager.getInstance().getMainWindow(), 
					(IElementSearchProvider<T>)ApplicationContext.getInstance().getRegisteredComponent(providerClass));
		}
		if(dialog.open()==ElementSelectionDialog.DIALOG_OK){
			selectedElement = dialog.getSelectedElement();
			// S'indica que hi ha repositori sel.leccionat
			inputText.setForeground(Color.BLACK);

			setText(selectedElement.toString());
			setToolTipText(selectedElement.toString());
		}
	}
}
