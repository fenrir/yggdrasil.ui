package org.fenrir.yggdrasil.ui.widget;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.AbstractCellEditor;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class ColorTableCellEditor extends AbstractCellEditor implements TableCellEditor 
{
	private static final long serialVersionUID = -8955851394667919469L;
	
	private JButton bColorChooser;
    private Color savedColor;
    
    public ColorTableCellEditor() 
    {
        ActionListener actionListener = new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent) 
            {
                Color color = JColorChooser.showDialog(bColorChooser, "Canviar color associat", savedColor);
                ColorTableCellEditor.this.setColor(color);
            }
        };
        bColorChooser = new JButton();
        bColorChooser.addActionListener(actionListener);
    }

    @Override
    public Object getCellEditorValue() 
    {
        return savedColor;
    }
    
    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) 
    {
        setColor((Color)value);
        return bColorChooser;
    }

    private void setColor(Color color) 
    {
        if(color!=null){
            savedColor = color;
            bColorChooser.setBackground(color);
        }
    }    
}
