package org.fenrir.yggdrasil.ui.widget;

import javax.swing.Icon;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class SelectableTreeNode extends DefaultMutableTreeNode 
{
	private static final long serialVersionUID = 7764946758228520946L;

	public final static int SINGLE_SELECTION = 0;

    public final static int DIG_IN_SELECTION = 1;

    private int state;
    private int selectionMode;
    private String name;
    private boolean orderable;
	
    private Icon[] icons;
	
    public SelectableTreeNode(Object userObject, String name, Icon openIcon, Icon closeIcon, boolean allowsChildren, boolean orderable, int state) 
    {
        super(userObject, allowsChildren);
        this.state = state;
        this.name = name;
        this.icons = new Icon[2];
        this.icons[0] = openIcon;
        this.icons[1] = closeIcon;
        this.orderable = orderable;
        setSelectionMode(DIG_IN_SELECTION);
    }
	
    public SelectableTreeNode(Object userObject, String name, Icon openIcon, Icon closeIcon, boolean allowsChildren, int state) 
    {
        this(userObject, name, openIcon, closeIcon, allowsChildren, !allowsChildren, state);
    }
	
    public SelectableTreeNode(Object userObject, String name, Icon icon, boolean allowsChildren, boolean selected) 
    {
        this(userObject, name, icon, icon, allowsChildren, selected ? TristateCheckBox.SELECTED : TristateCheckBox.NOT_SELECTED);
    }
	
    public SelectableTreeNode(Object userObject, String name, boolean allowsChildren, boolean selected) 
    {
        this(userObject, name, null, allowsChildren, selected);
    }
	
    public SelectableTreeNode(Object userObject, boolean allowsChildren, boolean selected) 
    {
        this(userObject, userObject.toString(), null, allowsChildren, selected);
    }

    public SelectableTreeNode(Object userObject, boolean allowsChildren) 
    {
        this(userObject, allowsChildren, false);
    }

    public SelectableTreeNode(Object userObject) 
    {
        this(userObject, true, false);
    }
	
    public SelectableTreeNode() 
    {
        this(new Object());
    }

    public void setSelectionMode(int mode) 
    {
        selectionMode = mode;
    }

    public int getSelectionMode() 
    {
        return selectionMode;
    }

    public int getState() 
    {
        return state;
    }

    public boolean isSelected() 
    {
        return state == TristateCheckBox.SELECTED;
    }


    public void setSelected(boolean b) 
    {
        setState(b ? TristateCheckBox.SELECTED : TristateCheckBox.NOT_SELECTED);
    }

    public void setOrderable(boolean b) 
    {
        this.orderable = b;
    }

    public boolean getOrderable() 
    {
        return this.orderable;
    }

    @Override
    public String toString() 
    {
        return name;
    }

    public Icon getIcon(boolean expanded) 
    {
        return icons[expanded ? 0 : 1];
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public void setIcon(Icon icon) 
    {
        this.icons[0] = icon;
        this.icons[1] = icon;
    }

    public void setIcon(Icon openIcon, Icon closeIcon) 
    {
        this.icons[0] = openIcon;
        this.icons[1] = closeIcon;
    }

    public void refresh() 
    {
        if(children!=null){
            Iterator it = children.iterator();
            while(it.hasNext()){
                SelectableTreeNode child = (SelectableTreeNode)it.next();
                child.refresh();
            }
            refreshState();
        }
    }

    public List getOrderableChildren() 
    {
        List result = new ArrayList();
        if(this.getOrderable()){
            result.add(this);
        } 
        else if(children!=null){
            Iterator iterator = children.iterator();
            while(iterator.hasNext()){
                SelectableTreeNode child = (SelectableTreeNode)iterator.next();
                result.addAll(child.getOrderableChildren());
            }
        } 
        else{
            SelectableTreeNode parentNode = (SelectableTreeNode)this.getParent();
            while(parentNode!=null){
                if(parentNode.getOrderable()){
                    result.add(parentNode);
                    break;
                }
                parentNode = (SelectableTreeNode)parentNode.getParent();
            }
        }

        return result;
    }

    private void setState(int state) 
    {
        this.state = state;

        if(selectionMode==DIG_IN_SELECTION){
            setChildrenState(this, state);
        }
        setParentsState(this);
    }

    private void setParentsState(SelectableTreeNode node) 
    {
        TreeNode parentNode = node.getParent();
        if(parentNode!=null && parentNode instanceof SelectableTreeNode){
            ((SelectableTreeNode)parentNode).refreshState();
            setParentsState(((SelectableTreeNode)parentNode));
        }
    }

    private void setChildrenState(SelectableTreeNode father, int state) 
    {
        if(father.children==null){
            return;
        }

        Iterator iterator = father.children.iterator();
        while(iterator.hasNext()){
            SelectableTreeNode node = (SelectableTreeNode)iterator.next();
            node.state = state;
            setChildrenState(node, state);
        }
    }

    private void refreshState() 
    {
        if(children!=null){
            Iterator iterator = children.iterator();
            int count = children.size();
            int selected = 0;
            while(iterator.hasNext()){
                SelectableTreeNode child = (SelectableTreeNode)iterator.next();
                if(child.state==TristateCheckBox.INDETERMINATE){
                    this.state = TristateCheckBox.INDETERMINATE;
                    return;
                }
                if(child.isSelected()){
                    selected++;
                }
            }
            if(selected==0){
                this.state = TristateCheckBox.NOT_SELECTED;
            }
            else if(selected<count){
                this.state = TristateCheckBox.INDETERMINATE;
            }
            else{
                this.state = TristateCheckBox.SELECTED;
            }
        }
    }

    protected void setName() 
    {
        Object value = getUserObject();
        setName(value.toString());
    }
}
