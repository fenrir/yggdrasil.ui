package org.fenrir.yggdrasil.ui.widget;

import javax.swing.Icon;
import java.util.UUID;
import org.fenrir.yggdrasil.ui.action.Action;

/**
 * TODO v1.0 TODO Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class TrayPopupEntry 
{
    // Id única de la notificació
    private String id;
    private String notificationId;
    private String type;
    private String message;
    private Icon icon;
    private Action action;

    public TrayPopupEntry()
    {
        // Cada entrada al popup de notificacions tindrá un ID diferent
        id = UUID.randomUUID().toString();  
    }
    
    /**
     * Cada entrada al popup de notificacions tindrá un ID diferent
     * @return String - ID únic de la notificació al popup de notificacions
     */
    public String getId()
    {
        return id;
    }
    
    public String getNotificationId()
    {
        return notificationId;
    }

    public void setNotificationId(String notificationId)
    {
        this.notificationId = notificationId;
    }

    public String getType()
    {
        return type;
    }

    public void setType(String type)
    {
        this.type = type;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public Icon getIcon()
    {
        return icon;
    }

    public void setIcon(Icon icon)
    {
        this.icon = icon;
    }

    public Action getAction()
    {
        return action;
    }

    public void setAction(Action action)
    {
        this.action = action;
    }
}
