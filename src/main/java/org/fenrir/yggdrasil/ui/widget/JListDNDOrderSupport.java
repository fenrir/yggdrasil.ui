package org.fenrir.yggdrasil.ui.widget;

import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DragGestureEvent;
import java.awt.dnd.DragGestureListener;
import java.awt.dnd.DragSource;
import java.awt.dnd.DragSourceDragEvent;
import java.awt.dnd.DragSourceDropEvent;
import java.awt.dnd.DragSourceEvent;
import java.awt.dnd.DragSourceListener;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.TransferHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public abstract class JListDNDOrderSupport extends TransferHandler implements DragSourceListener, DragGestureListener
{
	private static final long serialVersionUID = 7722576023417788800L;

	private final Logger log = LoggerFactory.getLogger(JListDNDOrderSupport.class);
    
    private JList list;
    private DragSource dragSource = new DragSource();
    
    public JListDNDOrderSupport(JList list)
    {
        this.list = list;        
        dragSource.createDefaultDragGestureRecognizer(list, DnDConstants.ACTION_MOVE, this);
    }
    
   @Override
    public void dragEnter(DragSourceDragEvent event) 
    {
        
    }

    @Override
    public void dragOver(DragSourceDragEvent event) 
    {
        
    }

    @Override
    public void dropActionChanged(DragSourceDragEvent event) 
    {
        
    }

    @Override
    public void dragExit(DragSourceEvent event) 
    {
        
    }

    @Override
    public void dragDropEnd(DragSourceDropEvent event) 
    {
        if(event.getDropSuccess()){            
            if(log.isDebugEnabled()){
                log.debug("Operació DND vàlida.");
            }            
        } 
        else{
            if(log.isDebugEnabled()){
                log.debug("Operació DND fallida");
            }
        }
    }

    @Override
    public void dragGestureRecognized(DragGestureEvent event) 
    {        
        StringSelection transferable = new StringSelection(Integer.toString(list.getSelectedIndex()));
        dragSource.startDrag(event, DragSource.DefaultCopyDrop, transferable, this);
    }    
   
    @Override
    public boolean canImport(TransferHandler.TransferSupport support) 
    {
        if(!support.isDataFlavorSupported(DataFlavor.stringFlavor)){
            return false;
        }
        JList.DropLocation dl = (JList.DropLocation)support.getDropLocation();
        return dl.getIndex()!=-1;        
    }

    @Override
    public boolean importData(TransferHandler.TransferSupport support) 
    {
        if(!canImport(support)){
            return false;
        }

        Transferable transferable = support.getTransferable();
        int sourceIndex;
        try {
            String strSourceIndex = (String)transferable.getTransferData(DataFlavor.stringFlavor);
            sourceIndex = Integer.parseInt(strSourceIndex);
        } 
        catch(Exception e){
            log.error("Error executant operació DND: {}", e.getMessage(), e);
            return false;
        }        
        JList.DropLocation dl = (JList.DropLocation)support.getDropLocation();
        int destinationIndex = dl.getIndex();
        // En el cas que es mogui a sota s'haurà de restar 1 a l'índex per compensar el que s'ha tret
        if(destinationIndex>sourceIndex){
            destinationIndex--;
        }
        
        onDrop(sourceIndex, destinationIndex);
        
        Object obj = ((DefaultListModel)list.getModel()).remove(sourceIndex);
        ((DefaultListModel)list.getModel()).add(destinationIndex, obj);                
                
        return true;
    }
    
    /**
     * Mètode a implementar a les subclasses que vulguin afegir una acció al gest Drag&Drop d'un element de la llista. 
     * S'executa just abans de modificar l'objecte JList, pel que les implementacions NO han de modificar la llista 
     * ja que es farà automàticament després de l'execució d'aquest mètode.
     * @param sourceIndex int - Índex de la llista origen del gest de Drag&Drop
     * @param destinationIndex - Índex de la llista destí del gest de Drag&Drop
     */
    public abstract void onDrop(int sourceIndex, int destinationIndex);
}
