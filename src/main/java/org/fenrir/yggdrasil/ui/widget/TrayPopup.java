package org.fenrir.yggdrasil.ui.widget;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JPanel;
import java.util.HashMap;
import java.util.Map;
import org.fenrir.yggdrasil.ui.ApplicationTray;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class TrayPopup extends JDialog
{
	private static final long serialVersionUID = 6552506355032544997L;
	
	public static final String ENTRY_TYPE_INFO_NOTIFICATION = "INFO_NOTIFICATION";
    public static final String ENTRY_TYPE_PROGRESS_NOTIFICATION = "PROGRESS_NOTIFICATION";
    
    private ApplicationTray tray;
    private Box pEntries;
    private Map<String, TrayPopupEntryWidget> entries = new HashMap<String, TrayPopupEntryWidget>();
    
    public TrayPopup(ApplicationTray tray)
    {
        this.tray = tray;        
        createContents();                
    }
    
    private void createContents()
    {
        setUndecorated(true);
        // Inicialment invisible
        setVisible(false);                
        
        JPanel pContents = new JPanel();
        // Directament no es pot afegir border a la finestra de dialeg, pel que es fa al panell de contingut
        pContents.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(5, 0, 5, 0)));
        
        pContents.setLayout(new BoxLayout(pContents, BoxLayout.Y_AXIS));
        // Opcions
        Box optionsPanel = Box.createHorizontalBox();
        optionsPanel.add(Box.createHorizontalGlue());
        JButton bDiscardAll = new JButton("Descartar totes");
        bDiscardAll.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent event) 
            {
                tray.discardAllNotifications();
            }
        });
        optionsPanel.add(bDiscardAll);
        // Marge esquerra
        optionsPanel.add(Box.createHorizontalStrut(5));
        optionsPanel.setPreferredSize(new Dimension(optionsPanel.getPreferredSize().width, 25));
        optionsPanel.setMaximumSize(new Dimension(optionsPanel.getMaximumSize().width, 25));
        pContents.add(optionsPanel);
        // Separador
        JSeparator separator = new JSeparator(JSeparator.HORIZONTAL);
        separator.setPreferredSize(new Dimension(separator.getPreferredSize().width, 10));
        separator.setMaximumSize(new Dimension(separator.getMaximumSize().width, 10));
        pContents.add(separator);
        // Panell amb les notifications
        pEntries = Box.createVerticalBox();
        pEntries.add(Box.createVerticalGlue());
        JScrollPane scrollEntries = new JScrollPane(pEntries);       
        scrollEntries.setBorder(BorderFactory.createEmptyBorder());
        pContents.add(scrollEntries);
        // S'afegeix tot el conjunt a la finestra de dialeg
        getContentPane().setLayout(new BorderLayout());
        getContentPane().add(pContents, BorderLayout.CENTER);
    }        
    
    public void addEntry(final TrayPopupEntry entry)
    {        
        TrayPopupEntryWidget widget = new TrayPopupEntryWidget(tray, entry);
        // Tamany
        int preferredHeight = widget.getPreferredSize().height;        
        widget.setPreferredSize(new Dimension(widget.getPreferredSize().width, preferredHeight));
        widget.setMaximumSize(new Dimension(widget.getMaximumSize().width, preferredHeight));
        pEntries.add(widget, 0);
        entries.put(entry.getId(), widget);
    }
    
    public void discardAllNotifications()
    {
        pEntries.removeAll();
        pEntries.repaint();
        entries.clear();
    }
    
    public void discardNotification(TrayPopupEntry entry)
    {
        TrayPopupEntryWidget widget = entries.remove(entry.getId());
        pEntries.remove(widget);
        /* Es reposicionen els elements al layout i es repinta el panell.
         * ja que en el cas que no hi hagi elements, en fer revalidate no ho fa
         */
        pEntries.revalidate();
        pEntries.repaint();
    }        
}
