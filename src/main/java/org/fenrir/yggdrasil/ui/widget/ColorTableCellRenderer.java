package org.fenrir.yggdrasil.ui.widget;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class ColorTableCellRenderer extends JLabel implements TableCellRenderer
{
	private static final long serialVersionUID = -2400166114442716450L;

	public ColorTableCellRenderer()
    {
        // Important!!
        setOpaque(true);
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object color, boolean isSelected, boolean hasFocus, int row, int column)
    {
        Color newColor = (Color)color;
        setBackground(newColor);        
        
        setToolTipText("RGB [" + newColor.getRed() + ", "
                + newColor.getGreen() + ", "
                + newColor.getBlue() + "]");
        
        return this;
    }
}
