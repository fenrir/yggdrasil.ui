package org.fenrir.yggdrasil.ui.widget;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.ButtonGroup;
import javax.swing.ButtonModel;
import javax.swing.Icon;
import javax.swing.JCheckBox;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeListener;
import javax.swing.plaf.ActionMapUIResource;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public class TristateCheckBox extends JCheckBox 
{
	private static final long serialVersionUID = -6758073133752506993L;
	
	public static final int NOT_SELECTED = 0;
    public static final int SELECTED = 1;
    public static final int INDETERMINATE = 2;
	
    private final TristateDecorator tristateModel;
	
    public TristateCheckBox(String text, Icon icon, int initial)
    {
        super(text, icon);

        super.addMouseListener(new MouseAdapter() 
        {
            @Override
            public void mousePressed(MouseEvent e) 
            {
                grabFocus();
                tristateModel.nextState();
            }
        });
		
        ActionMap map = new ActionMapUIResource();
        map.put("pressed", new AbstractAction() 
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                grabFocus();
                tristateModel.nextState();
            }
        });

        map.put("released", null);
        SwingUtilities.replaceUIActionMap(this, map);

        tristateModel = new TristateDecorator(getModel());
        setModel(tristateModel);
        setState(initial);
    }
	
    public TristateCheckBox(String text, int initial) 
    {
        this(text, null, initial);
    }
	
    public TristateCheckBox(String text) 
    {
        this(text, NOT_SELECTED);
    }
	
    public TristateCheckBox() 
    {
        this("");
    }
	  
    public void setState(int state) 
    { 
        tristateModel.setState(state); 
    }
	
    public int getState() 
    {
        return tristateModel.getState();
    }
	
    public void setChecked(boolean checked) 
    {
        if(checked){
            setState(SELECTED);
        }
        else{
            setState(NOT_SELECTED);
        }
    }

    public boolean getChecked() 
    {
        return tristateModel.getState()==SELECTED;
    }
	
    @Override
    public void addMouseListener(MouseListener l) 
    {		
        
    }
	
    private class TristateDecorator implements ButtonModel 
    {
        private final ButtonModel other;

        private TristateDecorator(ButtonModel other) 
        {
            this.other = other;
        }

        private void setState(int state) 
        {
            if (state == NOT_SELECTED) {
                other.setArmed(false);
                setPressed(false);
                setSelected(false);
            } 
            else if (state == SELECTED) {
                other.setArmed(false);
                setPressed(false);
                setSelected(true);
            } 
            else if (state == INDETERMINATE) {
                other.setArmed(true);
                setPressed(true);
                setSelected(true);
            }
        }

        private int getState() 
        {
            if(isSelected() && !isArmed()){
                return SELECTED;
            } 
            else if(isSelected() && isArmed()){
                return INDETERMINATE;
            } 
            else{
                return NOT_SELECTED;
            }
        }

        private void nextState() 
        {
            int current = getState();

            if(current==NOT_SELECTED){
                setState(SELECTED);
            } 
            else if(current==SELECTED){
                setState(INDETERMINATE);
            } 
            else if(current==INDETERMINATE){
                setState(NOT_SELECTED);
            }
        }

        @Override
        public void setArmed(boolean b) 
        {
            
        }

        @Override
        public void setEnabled(boolean b) 
        {
            setFocusable(b);
            other.setEnabled(b);
        }

        @Override
        public boolean isArmed() 
        {
            return other.isArmed();
        }

        @Override
        public boolean isSelected() 
        {
            return other.isSelected();
        }

        @Override
        public boolean isEnabled() {
            return other.isEnabled();
        }

        @Override
        public boolean isPressed() 
        {
            return other.isPressed();
        }

        @Override
        public boolean isRollover() 
        {
            return other.isRollover();
        }

        @Override
        public void setSelected(boolean b) 
        {
            other.setSelected(b);
        }

        @Override
        public void setPressed(boolean b) 
        {
            other.setPressed(b);
        }

        @Override
        public void setRollover(boolean b) 
        {
            other.setRollover(b);
        }

        @Override
        public void setMnemonic(int key) 
        {
            other.setMnemonic(key);
        }

        @Override
        public int getMnemonic() 
        {
            return other.getMnemonic();
        }

        @Override
        public void setActionCommand(String s) 
        {
            other.setActionCommand(s);
        }

        @Override
        public String getActionCommand() 
        {
            return other.getActionCommand();
        }

        @Override
        public void setGroup(ButtonGroup group) 
        {
            other.setGroup(group);
        }

        @Override
        public void addActionListener(ActionListener listener) 
        {
            other.addActionListener(listener);
        }

        @Override
        public void removeActionListener(ActionListener listener) 
        {
            other.removeActionListener(listener);
        }

        @Override
        public void addItemListener(ItemListener listener) 
        {
            other.addItemListener(listener);
        }

        @Override
        public void removeItemListener(ItemListener listener) 
        {
            other.removeItemListener(listener);
        }

        @Override
        public void addChangeListener(ChangeListener listener) 
        {
            other.addChangeListener(listener);
        }

        @Override
        public void removeChangeListener(ChangeListener listener) 
        {
            other.removeChangeListener(listener);
        }

        @Override
        public Object[] getSelectedObjects() 
        {
            return other.getSelectedObjects();
        }
    }
}
