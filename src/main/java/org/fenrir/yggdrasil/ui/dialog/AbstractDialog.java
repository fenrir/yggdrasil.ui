package org.fenrir.yggdrasil.ui.dialog;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.jdesktop.jxlayer.JXLayer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.widget.InProcessLayerUI;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20140826
 */
@SuppressWarnings("serial")
public abstract class AbstractDialog extends JDialog
{
    /** Tipus de missatge */
    public static final int MSG_TYPE_INFO = 1;
    public static final int MSG_TYPE_WARN = 2;
    public static final int MSG_TYPE_ERROR = 3;
    
    /** Tipus de retorn de la finestra de diàleg */
    public static final int DIALOG_CLOSE = 1;
    
    protected int dialogReturn = DIALOG_CLOSE;

    protected JXLayer layer;
    protected InProcessLayerUI layerUI;
    
    protected JLabel lIcon;
    protected JLabel lDescription;
    protected JLabel lMessage;
    protected JButton bClose;
    
    // Instancia del log
    private Logger log = LoggerFactory.getLogger(AbstractDialog.class);
    
    public AbstractDialog(Frame parent, String title, Dimension dimension, boolean modal)
    {
        super(parent, title, modal);

        JPanel pRoot = new JPanel();
        pRoot.setLayout(new BorderLayout());            	
    	JComponent pTitle = createTitlePanel(title);
    	JComponent pComponents = createContents();
    	JComponent pBotonera = createButtonPanel();
    	pRoot.add(pTitle, BorderLayout.NORTH);
    	pRoot.add(pComponents, BorderLayout.CENTER);
    	pRoot.add(pBotonera, BorderLayout.SOUTH);
        
        layer = new JXLayer(pRoot);          
        layerUI = new InProcessLayerUI();         
        layer.setUI(layerUI);
        getContentPane().add(layer);
        
    	setSize(dimension);

    	// Es centra al mig de la pantalla i es fa visible
    	Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    	setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 2);

    	addWindowListener(new WindowAdapter()
    	{
            @Override
            public void windowClosing(WindowEvent event)
            {
                if(log.isDebugEnabled()){
                    log.debug("Tancat finestra de dialeg a partir de WindowEvent");
                }
                close();
            }
    	});
    }
   
    public int open()
    {
    	setVisible(true);

    	return dialogReturn;
    }
    
    protected abstract JComponent createContents();

    protected JComponent createTitlePanel(String title)
    {
        JPanel pTitle = new JPanel();
        pTitle.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(200, 200, 200)));
        lIcon = new JLabel();
        lIcon.setHorizontalAlignment(JLabel.CENTER);
        lIcon.setVerticalAlignment(JLabel.CENTER);
        lDescription = new JLabel(title);
        lDescription.setFont(new Font("DejaVu Sans", Font.BOLD, 14));
        lMessage = new JLabel();

        GroupLayout layout = new GroupLayout(pTitle);
        pTitle.setLayout(layout);
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(lIcon, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(lDescription, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                    .addComponent(lMessage, GroupLayout.DEFAULT_SIZE, 495, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lDescription, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                .addComponent(lMessage, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
            )
            .addComponent(lIcon, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 60, Short.MAX_VALUE)
        );

        return pTitle;
    }
    
    protected JComponent createButtonPanel()
    {
        JPanel pBotonera = new JPanel();
        bClose = new JButton(new CloseAction());               
       
        GroupLayout layout = new GroupLayout(pBotonera);
        pBotonera.setLayout(layout);        
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(534, Short.MAX_VALUE)
                .addComponent(bClose)
                .addContainerGap()
            )
        );
        /* Grup Vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(bClose)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );
        
        return pBotonera;
    }
    
    protected void setInProcessMessage(String inProcessMessage)
    {
        layerUI.setMessage(inProcessMessage);
    }
    
    protected void showInProcess(boolean inProcess)
    {
        layerUI.setLocked(inProcess);
        layer.repaint();
    }
    
    public void setDescriptionMessage(String message)
    {
    	lDescription.setText(message);
    }
    
    /**
     * TODO v0.2 Afegir capacitat per posar icones
     * @param String message
     */
    public void setMessage(String message, int msgType)
    {
        if(MSG_TYPE_INFO==msgType){
            lMessage.setForeground(Color.BLACK);
        }
        else if(MSG_TYPE_WARN==msgType){
            lMessage.setForeground(Color.ORANGE);
        }
        else if(MSG_TYPE_ERROR==msgType){
            lMessage.setForeground(Color.RED);
        }


        lMessage.setText(message);
    }
    
    public void clearMessage()
    {
    	lMessage.setText(null);
    }
    
    protected void setIconAsResource(String iconPath)
    {
        ImageIcon icon = new ImageIcon(getClass().getResource(iconPath));
        lIcon.setIcon(icon);
    }
    
    protected abstract boolean onClose();
    
    public void close()
    {
    	if(onClose()){        	
    		dialogReturn = DIALOG_CLOSE;
            setVisible(false);
            dispose();
        }
    }
    
    protected void setCloseButtonEnabled(boolean enabled)
    {
    	bClose.setEnabled(enabled);
    }
    
    protected final class CloseAction extends AbstractAction
    {
    	public CloseAction()
    	{
            super("Tancar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {
        	if(log.isDebugEnabled()){
                log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
            }
        	
        	close();
        }
    }
}