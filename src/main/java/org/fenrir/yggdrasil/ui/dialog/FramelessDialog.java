package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.KeyStroke;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
@SuppressWarnings("serial")
public abstract class FramelessDialog extends JDialog
{    
    public static final String ACTION_MAP_KEY_CLOSE = "org.fenrir.yggdrasil.framework.ui.dialog.frameless.close"; 
    
    public FramelessDialog(Frame parent, Dimension dimension)
    {
        super(parent, false);
        setUndecorated(true);
        // Compte!! Això fa que sigui visible encara que es canvii d'aplicació
        setAlwaysOnTop(true);        
        
        setSize(dimension);

        JComponent contents = createContents();
        getContentPane().add(contents);
        getRootPane().setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
        
    	// Es centra al mig de la pantalla i es fa visible
    	Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    	setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 4);
        
        // Tancar la finestra amb la tecla ESC
        KeyStroke escapeStroke = KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0); 
        AbstractAction dispatchClosing = new AbstractAction() 
        { 
            @Override
            public void actionPerformed(ActionEvent event) 
            { 
                close();
            } 
        }; 
        getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeStroke, ACTION_MAP_KEY_CLOSE); 
        getRootPane().getActionMap().put(ACTION_MAP_KEY_CLOSE, dispatchClosing);         
    }        
    
    public void open()
    {
    	setVisible(true);
    }
    
    public void close()
    {
        setVisible(false);
        dispose();
    }
    
    protected abstract JComponent createContents();
}
