package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Map;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;
import org.apache.commons.lang.StringUtils;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.core.CoreConstants;
import org.fenrir.yggdrasil.core.service.IPreferenceService;

/**
 * TODO v1.0 Documentació
 * TODO v1.0 Internacionalització
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
@SuppressWarnings("serial")
public class WorkspaceDialog extends AbstractConfirmDialog 
{
    /* Components del formulari */
    private JTextField inputDirectori;
    private JCheckBox checkDefault;

    // Valor de retorn: Path al nou directori del workspace
    private String workspacePath = null;
    // Valor de retorn: Workspace per defecte
    private boolean defaultWorkspace = false;

    public WorkspaceDialog()
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), "Directori de treball", new Dimension(530, 220), true);

        setIconAsResource("/org/fenrir/yggdrasil/ui/icons/gear_48.png");
        initializeForm();
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        /* Definició dels components */
        JLabel lDirectori = new JLabel("Directori de treball:", SwingConstants.RIGHT);
        inputDirectori = new JTextField();
        inputDirectori.setEditable(false);
        JButton bBuscar = new JButton("Buscar");
        bBuscar.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent evt)
            {
                JFileChooser fileChooser = new JFileChooser(inputDirectori.getText());
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int retValue = fileChooser.showOpenDialog(WorkspaceDialog.this);
                if(retValue==JFileChooser.APPROVE_OPTION){
                    inputDirectori.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
            }
        });
        checkDefault = new JCheckBox("Utilitzar per defecte");
        checkDefault.setHorizontalAlignment(JCheckBox.RIGHT);

        /* Grup horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lDirectori, GroupLayout.PREFERRED_SIZE, 150, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(checkDefault)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    )
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(inputDirectori, GroupLayout.DEFAULT_SIZE, 275, Short.MAX_VALUE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bBuscar)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    )
                )
                .addGap(17, 17, 17)
            )
        );

        /* Grup vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(lDirectori, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                    .addComponent(inputDirectori, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                    .addComponent(bBuscar)
                )
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(checkDefault)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pContents;
    }

    private void initializeForm()
    {
        IPreferenceService preferenceService = (IPreferenceService)ApplicationContext.getInstance().getRegisteredComponent(IPreferenceService.class);

        String strWorkspaceDirectory = preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_CURRENT);
        inputDirectori.setText(strWorkspaceDirectory);
        
        if(StringUtils.isNotBlank(strWorkspaceDirectory)){
            Map<String, String> variables = new HashMap<String, String>();
            variables.put("folder", strWorkspaceDirectory);        
            boolean isDefaultWorkspace = IPreferenceService.CONFIGURATION_VALUE_TRUE.equals(preferenceService.getProperty(CoreConstants.PREFERENCES_WORKSPACE_IS_DEFAULT, variables));
            checkDefault.setSelected(isDefaultWorkspace);
        }
    }

    @Override
    protected boolean onAccept()
    {
        String strInputDirectori = inputDirectori.getText().trim();       
        if(StringUtils.isNotBlank(strInputDirectori)){
            // S'especifiquen el valors de retorn
            workspacePath = strInputDirectori;
            defaultWorkspace = checkDefault.isSelected();           
            
            return true;
        }
        else{
            setMessage("El camp DIRECTORI no pot ser buit", MSG_TYPE_ERROR);
            
            return false;
        }                
    }

    @Override
    protected boolean onCancel()
    {
        return true;
    }
    
    public String getWorkspacePath()
    {
    	return workspacePath;
    }
    
    public boolean isDefaultWorkspace()
    {
    	return defaultWorkspace;
    }
}