package org.fenrir.yggdrasil.ui.dialog;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import java.util.Collection;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizard;
import org.fenrir.yggdrasil.ui.wizard.AbstractWizardPage;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20140804
 */
@SuppressWarnings("serial")
public class WizardDialog<T extends AbstractWizard> extends AbstractDialog 
{
    /** Tipus de retorn de la finestra de diàleg */
    public static final int DIALOG_OK = 2;
    public static final int DIALOG_CANCEL = 3;
    public static final int DIALOG_FORWARD = 4;
    public static final int DIALOG_REWIND = 5;
    
    // Instancia del log
    private Logger log = LoggerFactory.getLogger(WizardDialog.class);
    
    protected JPanel pWizardPages;
    protected CardLayout pageLayout;
    
    protected JButton bFinalize;
    protected JButton bCancel;
    protected JButton bNext;
    protected JButton bPrevious;
    
    protected AbstractWizard wizard;

    public WizardDialog(Frame parent, String title, Dimension dimension, AbstractWizard wizard)
    {
        super(parent, title, dimension, true);

        this.wizard = wizard;
        String iconPath = wizard.getIcon();
        if(StringUtils.isNotBlank(iconPath)){
                setIconAsResource(iconPath);
        }
        wizard.setWizardDialog(this);
        // Es carreguen les pàgines del wizard
        Collection<AbstractWizardPage<? extends AbstractWizard>> vPages = wizard.getAllPages();
        for(AbstractWizardPage<? extends AbstractWizard> page:vPages){
            pWizardPages.add(page, page.getId());
        } 
		
        // Si el wizard només té una pàgina s'amaguen els botons << i >>
        if(wizard.getAllPages().size()<=1){
            bNext.setVisible(false);
            bPrevious.setVisible(false);
        }
		
        // Es posa la descripció de la pàgina actual
        String description = wizard.getCurrentPage().getDescription();
        setMessage(description, MSG_TYPE_INFO);

        // Es valida la pàgina inicial
        wizard.validateCurrentPage();
    }
    
    @Override
    protected JComponent createContents()
    {
    	pWizardPages = new JPanel();
    	pageLayout = new CardLayout();
    	pWizardPages.setLayout(pageLayout);    	    	   	    
    	
    	return pWizardPages;
    }
    
    @Override
    protected JComponent createButtonPanel()
    {
        JPanel pBotonera = new JPanel();
       
        bFinalize = new JButton(new FinalizeAction());
        bCancel = new JButton(new CancelAction());
        bNext = new JButton(new NextAction());
        bPrevious = new JButton(new PreviousAction());
        
        GroupLayout layout = new GroupLayout(pBotonera);
        pBotonera.setLayout(layout);
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(289, Short.MAX_VALUE)
                .addComponent(bPrevious)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bNext)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bFinalize)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bCancel)
                .addContainerGap()
            )
        );
        /* Grup Vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(bCancel)
                    .addComponent(bFinalize)
                    .addComponent(bNext)
                    .addComponent(bPrevious))
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );        

        return pBotonera;
    }
    
    @Override
    protected final void setCloseButtonEnabled(boolean enabled)
    {
    	
    }
    
    public void setForwardButtonEnabled(boolean enabled)
    {
    	bNext.setEnabled(enabled);
    }
    
    public void setBackwardButtonEnabled(boolean enabled)
    {
    	bPrevious.setEnabled(enabled);
    }
    
    public void setFinalizeButtonEnabled(boolean enabled)
    {
    	bFinalize.setEnabled(enabled);
    }
    
    public void setCancelButtonEnabled(boolean enabled)
    {
    	bCancel.setEnabled(enabled);
    }
    
    @Override
    protected final boolean onClose()
    {
        return true;
    }
    
    private class FinalizeAction extends AbstractAction
    {
    	public FinalizeAction()
    	{
            super("Finalitzar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {            
            if(wizard.performFinalize()){            	
                if(log.isDebugEnabled()){
                    log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
                }

                dialogReturn = DIALOG_OK;
                close();
            }  
        }
    }
    
    
    private class CancelAction extends AbstractAction
    {
    	public CancelAction()
    	{
            super("Cancelar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {         
            if(wizard.performCancel()){            	
                if(log.isDebugEnabled()){
                    log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
                }

                dialogReturn = DIALOG_CANCEL;
                close();
            }        	
        }
    }
    
    private class NextAction extends AbstractAction
    {
    	public NextAction()
    	{
            super("Següent");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {            
            if(wizard.performNext()){            	
                if(log.isDebugEnabled()){
                    log.debug("Carregant següent pàgina del wizard");
                }

                AbstractWizardPage<? extends AbstractWizard> nextPage = wizard.nextPage();
                setMessage(nextPage.getDescription(), MSG_TYPE_INFO);
                pageLayout.show(pWizardPages, nextPage.getId());
                wizard.validateCurrentPage();
            }  
        }
    }
    
    private class PreviousAction extends AbstractAction
    {
    	public PreviousAction()
    	{
            super("Anterior");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {            
            if(wizard.performPrevious()){            	
                if(log.isDebugEnabled()){
                    log.debug("Carregant pàgina anterior del wizard");
                }

                AbstractWizardPage<? extends AbstractWizard> previous = wizard.previousPage();
                setMessage(previous.getDescription(), MSG_TYPE_INFO);
                pageLayout.show(pWizardPages, previous.getId());
                wizard.validateCurrentPage();
            }  
        }
    }
}