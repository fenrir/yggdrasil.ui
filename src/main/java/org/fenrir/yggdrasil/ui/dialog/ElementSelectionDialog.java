package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultListModel;
import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.exception.ProviderException;
import org.fenrir.yggdrasil.core.provider.IElementSearchProvider;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131230
 */
public class ElementSelectionDialog<T> extends AbstractConfirmDialog 
{	
	private static final long serialVersionUID = 7746884239188023832L;

	private final Logger log = LoggerFactory.getLogger(ElementSelectionDialog.class);
	
	private IElementSearchProvider<T> provider;
	
	private JTextField inputName;
	private JList listElements;	
	
	private T selectedElement;
	
	public ElementSelectionDialog(Frame parent, IElementSearchProvider<T> provider)
	{
		super(parent, "Sel.leccionar element", new Dimension(450, 600), true);
		this.provider = provider;
		search();
	}
	
	public ElementSelectionDialog(Frame parent, IElementSearchProvider<T> provider, String term)
	{
		super(parent, "Sel.leccionar element", new Dimension(450, 600), true);
		this.provider = provider;
		
		inputName.setText(term);
		search();
	}
	
	@Override
    protected JComponent createContents()
    {
		JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);
        
        inputName = new JTextField();
        inputName.addKeyListener(new KeyAdapter() 
        {
			@Override
			public void keyReleased(KeyEvent event) 
			{
				try{
					search();
				}
				catch(Exception e){
					log.error("Error al buscar coincidències: " + e.getMessage(), e);
					JOptionPane.showMessageDialog(ElementSelectionDialog.this, "Error al buscar coincidències: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
				}
			}
        });
               
        DefaultListModel listModel = new DefaultListModel();
        listElements = new JList(listModel);    	                                        
		JScrollPane scrollTags = new JScrollPane(listElements);
		
		/* Grup horitzontal */
        layout.setHorizontalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                	.addComponent(inputName, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)                	
                    .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 337, Short.MAX_VALUE)
                )
                .addContainerGap()
            )
        );
        /* Grup vertical */
        layout.setVerticalGroup(
        	layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(inputName, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)                
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollTags, GroupLayout.DEFAULT_SIZE, 244, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
    }

	@Override
	protected boolean onAccept() 
	{				
		if(listElements.getSelectedIndex()<0){			
			setMessage("S'ha de sel.leccionar un element", MSG_TYPE_ERROR);			
			return false;
		}
		else{
			selectedElement = (T)listElements.getSelectedValue();
		}
		return true;
	}	
	
	@Override
	protected boolean onCancel()
	{
		return true;
	}
	
	public T getSelectedElement()
	{
		return selectedElement;
	}
	
	private void search()
	{
		try{
			DefaultListModel listModel = (DefaultListModel)listElements.getModel();
			listModel.removeAllElements();
			
			String term = inputName.getText();
			List<T> repositories;
			if(StringUtils.isNotBlank(term)){
				repositories = provider.findElementsNameLike(term);			
			}
			else{			
				repositories = provider.findAllElements();												
			}
			for(T repository:repositories){
				listModel.addElement(repository);
			}
		}
		catch(ProviderException e){
			log.error("Error buscant elements: " + e.getMessage(), e);
			JOptionPane.showMessageDialog(this, "Error buscant elements: " + e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
		}
	}		
}

