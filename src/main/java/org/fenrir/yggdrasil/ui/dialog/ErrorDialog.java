package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.LayoutStyle;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.swing.JTextPane;


/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20131203
 */
@SuppressWarnings("serial")
public class ErrorDialog extends AbstractDialog
{   
    protected static final int DIALOG_WIDTH = 500;
    protected static final int COLLAPSED_HEIGHT = 200;
    protected static final int EXPANDED_HEIGHT = 600;
    
    protected JToggleButton bDetail;
    protected JLabel errorMessage;
    protected JTextPane errorDetail;
    protected JScrollPane scrollDetail;
    protected String message;
    protected Throwable error;
    
    public ErrorDialog(Frame parent, String message, Throwable error)
    {
        super(parent, "Error", new Dimension(DIALOG_WIDTH, COLLAPSED_HEIGHT), true);
        setIconAsResource("/org/fenrir/yggdrasil/ui/icons/error_48.png");
        setResizable(false);
        // Es transforma el missatge per fer wrapping en cas de desbordament
        String htmlMessage = "<html>" + message + "</html>";
        errorMessage.setText(htmlMessage);
        
        StringWriter writer = new StringWriter();  
        error.printStackTrace(new PrintWriter(writer));  
        String stacktraceMessage = writer.toString();
        errorDetail.setText(stacktraceMessage);
    }

    @Override
    protected JComponent createContents() 
    {
        JPanel pContents = new JPanel();
        GroupLayout layout = new GroupLayout(pContents);
        pContents.setLayout(layout);

        errorMessage = new JLabel();
        errorDetail = new JTextPane();
        errorDetail.setEditable(false);
        scrollDetail = new JScrollPane(errorDetail);
        scrollDetail.setVisible(false);
        
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                    .addComponent(errorMessage)
                    .addComponent(scrollDetail)
                )
                .addContainerGap()
            )
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()                
                .addComponent(errorMessage, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(scrollDetail, GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addContainerGap()
            )
        );
        
        return pContents;
    }
    
    @Override
    protected JComponent createButtonPanel()
    {
        JPanel pBotonera = new JPanel();
       
        bDetail = new JToggleButton(new ToggleDetailAction());
        bClose = new JButton(new AbstractDialog.CloseAction()); 
        
        GroupLayout layout = new GroupLayout(pBotonera);
        pBotonera.setLayout(layout);
        // Si s'ha de mostrar només el botó de tancar (bAccept)                
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(462, Short.MAX_VALUE)
                .addComponent(bDetail)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bClose)
                .addContainerGap()
            )
        );
        /* Grup Vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(bDetail)
                    .addComponent(bClose)
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pBotonera;
    }	    
    
    @Override
    protected final boolean onClose()
    {
        return true;
    }
    
    private class ToggleDetailAction extends AbstractAction
    {
    	public ToggleDetailAction()
    	{
            super("Detalls");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {
            JToggleButton srcButton = (JToggleButton)event.getSource();
            int height = srcButton.isSelected() ? EXPANDED_HEIGHT : COLLAPSED_HEIGHT;            
            setSize(DIALOG_WIDTH, height);            
            // Es centra al mig de la pantalla
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((d.width - getWidth()) / 2, (d.height - getHeight()) / 2);
            scrollDetail.setVisible(srcButton.isSelected());            
        }
    }
}
