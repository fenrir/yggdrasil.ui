package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Dimension;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.Box;
import java.util.List;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.event.IWorkerUpdateListener;
import org.fenrir.yggdrasil.ui.worker.AbstractWorker;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionalitzar missatges
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
@SuppressWarnings("serial")
public class TaskProgressDialog extends AbstractDialog implements IWorkerUpdateListener<String>  
{
    private AbstractWorker task;
    private JProgressBar progressBar;

    public TaskProgressDialog(String taskTitle, AbstractWorker task) throws Exception
    {
       	this(taskTitle, task, new Dimension(450, 180));			
    }
    
    public TaskProgressDialog(String taskTitle, AbstractWorker task, Dimension dimension) throws Exception
    {
        super(ApplicationWindowManager.getInstance().getMainWindow(), taskTitle, dimension, true);		

        task.addProcessListener(this);
        this.task = task;
    }

    @Override
    protected JComponent createContents()
    {
        JPanel pContents = new JPanel();
        BoxLayout layout = new BoxLayout(pContents, BoxLayout.Y_AXIS);
        pContents.setLayout(layout);

        Box pProgress = Box.createHorizontalBox();
        pProgress.add(Box.createHorizontalStrut(15));
        
        progressBar = new JProgressBar(0, 100);
        progressBar.setPreferredSize(new Dimension(Short.MAX_VALUE, 30));
        progressBar.setIndeterminate(true);
        pProgress.add(progressBar);
        
        pProgress.add(Box.createHorizontalStrut(15));
        pContents.add(pProgress);

        return pContents;
    }

    @Override
    public int open() 
    {
        task.execute();		
        setCloseButtonEnabled(false);

        return super.open();
    }

    @Override
    protected boolean onClose() 
    {
        return true;
    }

    @Override
    public void process(List<String> chunks) 
    {	
        if(!chunks.isEmpty()){
           progressBar.setString(chunks.get(0));
        }
    }

    @Override
    public void done() 
    {
        progressBar.setIndeterminate(false);
        progressBar.setValue(100);
        setMessage("Process finalitzat", MSG_TYPE_INFO);

        // Es torna a deixar tancar la finestra
        setCloseButtonEnabled(true);
    }	
}
