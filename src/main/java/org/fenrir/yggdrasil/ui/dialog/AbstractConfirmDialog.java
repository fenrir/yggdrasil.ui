package org.fenrir.yggdrasil.ui.dialog;

import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * TODO v1.0 Javadoc
 * TODO v1.0 Internacionlitzar
 * @author Antonio Archilla Nava
 * @version v0.1.20140826
 */
@SuppressWarnings("serial")
public abstract class AbstractConfirmDialog extends AbstractDialog
{
    /** Tipus de retorn de la finestra de diàleg */
    public static final int DIALOG_OK = 2;
    public static final int DIALOG_CANCEL = 3;
	
    // Instancia del log
    private Logger log = LoggerFactory.getLogger(AbstractConfirmDialog.class);
	
    protected JButton bAccept;
    protected JButton bCancel;
    
    public AbstractConfirmDialog(Frame parent, String title, Dimension dimension, boolean modal)
    {
        super(parent, title, dimension, modal);
    }
	
    @Override
    protected JComponent createButtonPanel()
    {
        JPanel pBotonera = new JPanel();
       
        bAccept = new JButton(new AcceptAction());
        bCancel = new JButton(new CancelAction());
        
        GroupLayout layout = new GroupLayout(pBotonera);
        pBotonera.setLayout(layout);
        // Si s'ha de mostrar només el botó de tancar (bAccept)                
        /* Grup Horitzontal */
        layout.setHorizontalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(462, Short.MAX_VALUE)
                .addComponent(bAccept)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(bCancel)
                .addContainerGap()
            )
        );
        /* Grup Vertical */
        layout.setVerticalGroup(
            layout.createParallelGroup(GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                    .addComponent(bAccept)
                    .addComponent(bCancel)
                )
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            )
        );

        return pBotonera;
    }
	
    @Override
    protected final void setCloseButtonEnabled(boolean enabled)
    {
    	
    }
	
    protected void setAcceptButtonEnabled(boolean enabled)
    {
    	bAccept.setEnabled(enabled);
    }    	
    
    protected void setCancelButtonEnabled(boolean enabled)
    {
    	bCancel.setEnabled(enabled);    	
    }
	
    @Override
    protected final boolean onClose()
    {
        return false;
    }
    
    /**
     * ATENCIÓ: La funció de tancament és CANCEL en comptes de CLOSE com a la classe AbstractDialog
     */
    public void close()
    {
    	if(onCancel()){
    		dialogReturn = DIALOG_CANCEL;
            setVisible(false);
            dispose();
        }
    }
	
    protected abstract boolean onAccept();

    protected abstract boolean onCancel();
    
    protected final class AcceptAction extends AbstractAction
    {
    	public AcceptAction()
    	{
            super("Acceptar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {            
            if(onAccept()){            	
                if(log.isDebugEnabled()){
                    log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
                }
		
                dialogReturn = DIALOG_OK;
                setVisible(false);
                dispose();
            }
        }
    }
    
    protected final class CancelAction extends AbstractAction
    {
    	public CancelAction()
    	{
            super("Cancel.lar");
    	}
    	
        @Override
        public void actionPerformed(ActionEvent event)
        {            
            if(onCancel()){            	
                if(log.isDebugEnabled()){
                    log.debug("Tancant finestra de dialeg a partir d'un ActionEvent");
                }
	
                dialogReturn = DIALOG_CANCEL;
                setVisible(false);
                dispose();
            }
        }
    }
}