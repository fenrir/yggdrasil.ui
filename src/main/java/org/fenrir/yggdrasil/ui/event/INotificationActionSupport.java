package org.fenrir.yggdrasil.ui.event;

import org.fenrir.yggdrasil.ui.action.Action;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public interface INotificationActionSupport 
{
    public Action getAction();
}
