package org.fenrir.yggdrasil.ui.event;

import java.util.List;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20131205
 */
public interface IWorkerUpdateListener<T> 
{
    public void process(List<T> chunks);
    public void done();
}
