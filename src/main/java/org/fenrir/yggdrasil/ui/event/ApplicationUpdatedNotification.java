package org.fenrir.yggdrasil.ui.event;

import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.ui.action.DownloadUpdatesAction;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
public class ApplicationUpdatedNotification implements IApplicationNotification, INotificationActionSupport
{           
    public static final String NOTIFICATION_ID = NotificationEventConstants.NOTIFICATION_APPLICATION_UPDATED;
    
    @Override
    public String getId()
    {
        return NOTIFICATION_ID;
    }
        
    @Override
    public String getCaption() 
    {
        return "Tasca de comprobació d'actualitzacions";        
    }
    
    @Override
    public final void setCaption(String caption)
    {
        
    }

    @Override
    public String getMessage() 
    {
        return "S'han trobat actualitzacions per l'aplicació";
    }
    
    @Override
    public final void setMessage(String message)
    {
        
    }

    @Override
    public String getIconPath() 
    {
        return "/org/fenrir/yggdrasil/ui/icons/upgrade_22.png";
    }
    
    @Override
    public final void setIconPath(String iconPath)
    {
        
    }

    @Override
    public Action getAction()
    {
        return new DownloadUpdatesAction();
    }

    @Override
    public final void setParameter(String id, Object value) 
    {
        
    }
}
