package org.fenrir.yggdrasil.ui.event;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import javax.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.event.IWindowListener;
import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.service.IEventNotificationService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20130315
 */
public final class ApplicationWindowListener implements WindowListener 
{
    private final Logger log = LoggerFactory.getLogger(ApplicationWindowListener.class);
	
    private AbstractApplication applicationInstance;
	
    @Inject
    private IEventNotificationService eventNotificationService;
    
    public void setApplicationInstance(AbstractApplication application)
    {
        this.applicationInstance = application;
    }
    
    public void setEventNotificationService(IEventNotificationService eventNotificationService)
    {
        this.eventNotificationService = eventNotificationService;
    }
	
    public void preWindowOpen()
    {
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_PRE_WINDOW_OPEN_ID);
        }
        catch(Exception e){
            log.error("Error notificant event pre-obertura de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de pre-obertura de la finestra: " + e.getMessage(), e);
        }
    }
		
    @Override
    public void windowOpened(WindowEvent event) 
    {
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_OPENED_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event post-obertura de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de post-obertura de la finestra: " + e.getMessage(), e);
        }
    }
	
    @Override
    public void windowActivated(WindowEvent event) 
    {
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_ACTIVATED_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event d'activació de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event d'activació de la finestra: " + e.getMessage(), e);
        }
    }
    
    @Override
    public void windowDeactivated(WindowEvent event) 
    {	
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_DEACTIVATED_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event de desactivació de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de desactivació de la finestra: " + e.getMessage(), e);
        }
    }

    @Override
    public void windowClosing(WindowEvent event) 
    {	
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_CLOSING_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event de tancament de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de tancament de la finestra: " + e.getMessage(), e);
        }
    }    
    
    @Override
    public void windowClosed(WindowEvent event) 
    {	
        try{
            applicationInstance.stop();
            
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_CLOSED_ID, event);
        }
        catch(ApplicationException e){
            log.error("Error aturant l'aplicació: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error aturant l'aplicació: " + e.getMessage(), e);
        }
        catch(Exception e){
            log.error("Error notificant event de finestra tancada: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event de tancament de la finestra: " + e.getMessage(), e);
        }
        finally{
            System.exit(0);
        }
    }    

    @Override
    public void windowDeiconified(WindowEvent event) 
    {	
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_DECONIFIED_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event de deiconificació de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event deiconificació de la finestra: " + e.getMessage(), e);
        }
    }

    @Override
    public void windowIconified(WindowEvent event) 
    {	
        try{
            eventNotificationService.notifyNamedEvent(IWindowListener.class, IWindowListener.EVENT_WINDOW_ICONIFIED_ID, event);
        }
        catch(Exception e){
            log.error("Error notificant event d'iconificació de la finestra: {}", e.getMessage(), e);
            ApplicationWindowManager.getInstance().displayErrorMessage("Error notificant event d'iconificació de la finestra: " + e.getMessage(), e);
        }
    }
}