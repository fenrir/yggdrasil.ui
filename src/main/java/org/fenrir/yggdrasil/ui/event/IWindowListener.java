package org.fenrir.yggdrasil.ui.event;

import java.awt.event.WindowEvent;
import org.fenrir.yggdrasil.core.event.annotation.EventMethod;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
public interface IWindowListener 
{
    public static final String EVENT_PRE_WINDOW_OPEN_ID = "org.fenrir.yggdrasil.event.ui.preWindowOpen";
    public static final String EVENT_WINDOW_OPENED_ID = "org.fenrir.yggdrasil.event.ui.windowOpened";
    public static final String EVENT_WINDOW_ACTIVATED_ID = "org.fenrir.yggdrasil.event.ui.windowActivated";
    public static final String EVENT_WINDOW_CLOSED_ID = "org.fenrir.yggdrasil.event.ui.windowClosed";
    public static final String EVENT_WINDOW_CLOSING_ID = "org.fenrir.yggdrasil.event.ui.windowClosing";
    public static final String EVENT_WINDOW_DEACTIVATED_ID = "org.fenrir.yggdrasil.event.ui.windowDeactivated";
    public static final String EVENT_WINDOW_DECONIFIED_ID = "org.fenrir.yggdrasil.event.ui.windowDeconified";
    public static final String EVENT_WINDOW_ICONIFIED_ID = "org.fenrir.yggdrasil.event.ui.windowIconified";
    
    @EventMethod(eventName=EVENT_PRE_WINDOW_OPEN_ID)
    public void preWindowOpen();
    
    @EventMethod(eventName=EVENT_WINDOW_OPENED_ID)
    public void windowOpened(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_ACTIVATED_ID)
    public void windowActivated(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_CLOSED_ID)
    public void windowClosed(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_CLOSING_ID)
    public void windowClosing(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_DEACTIVATED_ID)
    public void windowDeactivated(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_DECONIFIED_ID)
    public void windowDeiconified(WindowEvent event);
    
    @EventMethod(eventName=EVENT_WINDOW_ICONIFIED_ID)
    public void windowIconified(WindowEvent event);
}
