package org.fenrir.yggdrasil.ui.event;

import org.fenrir.yggdrasil.ui.ApplicationWindowManager;
import org.fenrir.yggdrasil.ui.action.Action;
import org.fenrir.yggdrasil.core.event.IApplicationNotification;
import org.fenrir.yggdrasil.core.event.NotificationEventConstants;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
public class ApplicationErrorNotification implements IApplicationNotification, INotificationActionSupport
{
    public static final String NOTIFICATION_ID = NotificationEventConstants.NOTIFICATION_ERROR;    
    
    private String message;
    private String errorDescription;
    private Throwable errorThrowable;
        
    @Override
    public String getId()
    {
        return NOTIFICATION_ID;
    }
    
    @Override
    public String getCaption() 
    {
        return "S'ha produit un error a l'execució de l'aplicació";
    }

    @Override
    public final void setCaption(String caption) 
    {
        
    }

    @Override
    public String getMessage() 
    {
        return message;
    }

    @Override
    public void setMessage(String message) 
    {
        this.message = message;
    }

    @Override
    public String getIconPath() 
    {
        return "/org/fenrir/yggdrasil/ui/icons/error_22.png";
    }

    @Override
    public final void setIconPath(String iconPath) 
    {
        // No cal implementació. Icona fixe
    }

    @Override
    public Action getAction()
    {
        // S'obren les incidències actualitzades a la llista de la vista principal
        Action action = new Action() 
        {
            @Override
            public void actionPerformed() 
            {
                ApplicationWindowManager.getInstance().displayErrorMessage(errorDescription, errorThrowable);
            }
        };
        
        return action;
    }

    @Override
    public void setParameter(String id, Object value) 
    {
        if(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_DESCRIPTION.equals(id)){
            errorDescription = (String)value;
        }
        else if(NotificationEventConstants.NOTIFICATION_PARAMETER_ERROR_THROWABLE.equals(id)){
            errorThrowable = (Throwable)value;
        }
        else{
            throw new IllegalArgumentException("La ID especificada no es vàlida");
        }        
    }
}
