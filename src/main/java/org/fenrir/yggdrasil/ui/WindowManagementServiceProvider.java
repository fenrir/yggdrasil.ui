package org.fenrir.yggdrasil.ui;

import org.fenrir.yggdrasil.core.AbstractApplication;
import org.fenrir.yggdrasil.core.exception.ApplicationException;
import org.fenrir.yggdrasil.core.extension.IWindowManagementService;

/**
 * TODO v1.0 Javadoc
 * @author Antonio Archilla Nava
 * @version v0.1.20130315
 */
public class WindowManagementServiceProvider implements IWindowManagementService 
{
	@Override
	public void initialize(AbstractApplication application) 
	{
		ApplicationWindowManager.getInstance().initialize(application);
	}

	@Override
	public void notifyPreWindowOpenEvent()
	{
		ApplicationWindowManager.getInstance().notifyPreWindowOpenEvent();
	}
	
	@Override
	public void displayErrorMessage(String message, Throwable error) 
	{
		ApplicationWindowManager.getInstance().displayErrorMessage(message, error);
	}

	@Override
	public void displayStandaloneErrorMessage(String message, Throwable error) 
	{
		ApplicationWindowManager.displayStandaloneErrorMessage(message, error);
	}

	@Override
	public void createWindow() throws ApplicationException 
	{
		try{
			ApplicationWindowManager.getInstance().createWindow();
		}
		catch(Exception e){
			throw new ApplicationException("S'ha produit un error durant la creació de la finestra principal", e);
		}
	}

	@Override
	public void showWindow() 
	{
		ApplicationWindowManager.getInstance().showWindow();
	}
}
