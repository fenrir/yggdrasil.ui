package org.fenrir.yggdrasil.ui;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.SplashScreen;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.fenrir.yggdrasil.core.extension.ISplashScreenManagementService;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140315
 */
public final class SplashScreenManager implements ISplashScreenManagementService
{
    private static final Logger log = LoggerFactory.getLogger(SplashScreenManager.class);
    
    private SplashScreen splash;
    private Graphics2D splashGraphics;
    
    public SplashScreenManager()
    {
        // Inicialització de l'splash-screen
        splash = SplashScreen.getSplashScreen();
        if(splash!=null){
            splashGraphics = splash.createGraphics();        
        }
        else{
            log.warn("No s'ha pogut obtenir la splash-screen");
        }
    }
    
    /**
     * TODO Calcular les coordenades del text i la progressbar
     * @param progress
     * @param message 
     */
    @Override
    public void drawSplashScreenProgress(int progress, String message)
    {
        if(splash!=null){
            // Es neteja la superfície abans d'escriure-hi            
            Dimension splashSize = splash.getSize();
            splashGraphics.setComposite(AlphaComposite.Clear);
            splashGraphics.fillRect(0, 0, splashSize.width, splashSize.height);
            // Es pinta el progrés
            splashGraphics.setPaintMode();            
            splashGraphics.setColor(Color.BLACK);
            splashGraphics.drawString(message, 5, 400);
            splashGraphics.setColor(Color.RED);
            int progressWidth = splashSize.width / 100 * progress;
            splashGraphics.fillRect(0, 410, progressWidth, 2);

            splash.update();
        }
    }
    
    @Override
    public void closeSplashScreen()
    {
        if(splash!=null){
            splash.close();
            splash = null;
        }
    }
}
