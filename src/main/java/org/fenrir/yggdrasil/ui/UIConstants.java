package org.fenrir.yggdrasil.ui;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20141011
 */
public class UIConstants 
{
	/*--------------------------*
	 *  DEFAULT VALUES  * 
	 *--------------------------*/
	public static final String DEFAULT_APPLICATION_ICON_22 = "/org/fenrir/yggdrasil/ui/icons/tray_icon_22.png";
	
	/*--------------*
	 * PREFERENCIES * 
	 *--------------*/
	public static final String PREFERENCES_UI_DEFAULT_PERSPECTIVE = "//preferences[@id='org.fenrir.yggdrasil.ui']/ui/defaultPerspective";
	public static final String PREFERENCES_UI_LOOK_AND_FEEL_SKIN = "//preferences[@id='org.fenrir.yggdrasil.ui']/ui/lookandfeel/skin";
}
