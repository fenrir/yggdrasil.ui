package org.fenrir.yggdrasil.ui.mvc;

import javax.swing.JComponent;
import javax.swing.JPanel;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140722
 */
@SuppressWarnings("serial")
public class EmptyPerspective extends AbstractPerspective 
{
	public static final String ID = "org.fenrir.yggdrasil.ui.perspective.emptyPerspective";
	
	@Override
	protected JComponent createPerspectiveContents() 
	{
		return new JPanel();
	}

	@Override
	public String getId() 
	{
		return ID;
	}

	@Override
	public boolean isViewAllowed(String viewId) 
	{
		return false;
	}
}
