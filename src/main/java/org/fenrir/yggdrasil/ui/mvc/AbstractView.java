package org.fenrir.yggdrasil.ui.mvc;

import java.awt.BorderLayout;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JToolBar;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131227
 */
@SuppressWarnings("serial")
public abstract class AbstractView<T extends AbstractController<? extends AbstractModel>> extends JPanel
{
    public static final String TOOLBAR_LOCATION_NORTH = BorderLayout.NORTH;
    public static final String TOOLBAR_LOCATION_SOUTH = BorderLayout.SOUTH;
    public static final String TOOLBAR_LOCATION_EAST = BorderLayout.EAST;
    public static final String TOOLBAR_LOCATION_WEST = BorderLayout.WEST;	
	
    protected T controller;
	
    protected JToolBar viewToolBar;
	
    public abstract String getId();
    
    public String getComposedId()
    {
    	return getId();
    }
    
    public final void createView()
    {
        // TODO S'ha de llançar una excepció si la vista ja ha estat creada?
        setLayout(new BorderLayout());
        JComponent panel = createViewContents();
        add(panel, BorderLayout.CENTER);
    }
    
    protected abstract JComponent createViewContents();

    protected final void createViewToolBar(String location)
    {
        if(TOOLBAR_LOCATION_EAST.equals(location) || TOOLBAR_LOCATION_WEST.equals(location)){
            viewToolBar = new JToolBar(JToolBar.VERTICAL);
        }
        else if(TOOLBAR_LOCATION_NORTH.equals(location) || TOOLBAR_LOCATION_SOUTH.equals(location)){
            viewToolBar = new JToolBar(JToolBar.HORIZONTAL);
        }
        else{
            throw new IllegalArgumentException("La localització de la barra d'eines de la vista es incorrecte");
        }
		
        add(viewToolBar, location);
    }
	
    public void setController(T controller)
    {
        this.controller = controller;
    }
	
    public void addToolbarAction(AbstractAction action)
    {
        viewToolBar.add(action);
    }    
}