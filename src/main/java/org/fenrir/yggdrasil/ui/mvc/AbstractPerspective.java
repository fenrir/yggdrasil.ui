package org.fenrir.yggdrasil.ui.mvc;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JComponent;
import javax.swing.JPanel;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import org.fenrir.yggdrasil.core.ApplicationContext;
import org.fenrir.yggdrasil.ui.ApplicationWindowManager;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20140724
 */
@SuppressWarnings("serial")
public abstract class AbstractPerspective extends JPanel
{
	protected HashMap<String, AbstractPerspective.ViewPlaceHolder> viewPlaceHolders = new HashMap<String, AbstractPerspective.ViewPlaceHolder>();
	
	public abstract String getId();
	
	public final void createPerspective()
	{
		// TODO S'ha de llançar una excepció si la perspectiva ja ha estat creada?
        setLayout(new BorderLayout());
        JComponent panel = createPerspectiveContents();
        add(panel, BorderLayout.CENTER);
	}
	
	protected abstract JComponent createPerspectiveContents(); 
	
	public void initializePerspective()
	{
		
	}
	
	public abstract boolean isViewAllowed(String viewId);
	
	protected ViewPlaceHolder getViewPlaceHolder(String viewId)
	{
		return viewPlaceHolders.get(viewId);
	}
	
	protected void setViewPlaceHolder(String viewId, ViewPlaceHolder viewPlaceHolder)
	{
		viewPlaceHolders.put(viewId, viewPlaceHolder);
	}
	
	public AbstractView<?> getView(String id) 
	{
		return ApplicationWindowManager.getInstance().getViewFromRegistry(id);
	}
    
	public <T extends AbstractView<?>> void addView(String id, Class<T> viewClass)
    {
    	T view = (T)ApplicationContext.getInstance().getRegisteredComponent(viewClass);
    	addView(id, view);
    }
	
	public void addView(String id, AbstractView<?> view) 
	{
		if(isViewAllowed(view.getId())){
			if(getView(id)==null){
				ApplicationWindowManager.getInstance().addViewToRegistry(id, view);
				getViewPlaceHolder(view.getId()).addView(id, view);
			}
		}
	}
	
	public void viewRemoved(AbstractView<?> view) 
	{
		if(isViewAllowed(view.getId())){
			getViewPlaceHolder(view.getId()).removeView(view);
		}
	}
    
	public void switchToView(String viewID) 
	{
		AbstractView<?> view = getView(viewID);
		if(view!=null){
			ViewPlaceHolder placeHolder = getViewPlaceHolder(view.getId());
			if(!placeHolder.containsView(viewID)){
				placeHolder.addView(viewID, view);
			}
			getViewPlaceHolder(view.getId()).switchToView(viewID);
		}
	}
	
	protected class ViewPlaceHolder
	{
		private JPanel contentPanel;
		private CardLayout layout;
		
		private Set<String> placedViews = new HashSet<String>();
		
		public ViewPlaceHolder()
		{
			layout = new CardLayout();
			contentPanel = new JPanel(layout);
		}
		
		public JPanel getContentPanel()
		{
			return contentPanel;
		}
		
		boolean containsView(String id)
		{
			return placedViews.contains(id);
		}
		
		void addView(String id, AbstractView<?> view)
	    {
			placedViews.add(id);
	    	contentPanel.add(id, view);
	    }
	    
	    void removeView(AbstractView<?> view)
	    {
	    	placedViews.remove(view.getComposedId());
	    	contentPanel.remove(view);
	    }
		
		void switchToView(String viewID)
	    {
	        layout.show(contentPanel, viewID);        
	    }
	}
}
