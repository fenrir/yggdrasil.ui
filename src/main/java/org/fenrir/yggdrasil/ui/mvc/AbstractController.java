package org.fenrir.yggdrasil.ui.mvc;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * TODO v1.0 Documentació
 * @author Antonio Archilla Nava
 * @version v0.1.20131215
 */
public abstract class AbstractController<T extends AbstractModel> implements PropertyChangeListener
{	
    protected T model;
	
    public AbstractController()
    {
		
    }
	
    public AbstractController(T model)
    {
        setModel(model);
    }
	
    @SuppressWarnings("rawtypes")
    public AbstractController(T model, AbstractView view)
    {
        setModel(model);
        attachView(view);
    }
		
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public void attachView(AbstractView view)
    {
        view.setController(this);
    }
	
    public void setModel(T model)
    {
        this.model = model;
        model.addPropertyChangeListener(this);
    }
	
    @Override
    public void propertyChange(PropertyChangeEvent event) 
    {	
		
    }
}